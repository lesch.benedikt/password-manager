import 'package:flutter/material.dart';
import 'package:kee_pass_manager/utils/password_generator.dart';

class PasswordStrengthIndicator extends StatelessWidget {
  final String password;

  PasswordStrengthIndicator(this.password);

  @override
  Widget build(BuildContext context) {
    int strength = PasswordGenerator.getPasswordStrength(password);

    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        child: LinearProgressIndicator(
          value: mapValue(strength),
          color: getColor(strength),
          minHeight: 8,
        ),
      ),
    );
  }

  mapValue(int strength) {
    return strength / 9;
  }

  getColor(int strength) {
    if (strength < 5) return Colors.red;
    if (strength < 8) return Colors.yellow;
    if (strength >= 8) return Colors.green;
  }
}
