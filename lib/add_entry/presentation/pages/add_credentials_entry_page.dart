import 'dart:convert';

import 'package:encrypt/encrypt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/add_entry/presentation/widgets/password_strength_indicator.dart';
import 'package:kee_pass_manager/common/presentation/widgets/default_bloc_consumer.dart';
import 'package:kee_pass_manager/common/presentation/widgets/dense_text_fom_field.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credentials_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/use_cases/database_state.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:kee_pass_manager/utils/password_generator.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';
import 'package:uuid/uuid.dart';

class AddCredentialsEntryPage extends StatefulWidget {
  final CredentialsEntry? entry;

  AddCredentialsEntryPage([this.entry]);

  @override
  _AddCredentialsEntryPageState createState() =>
      _AddCredentialsEntryPageState(entry);
}

class _AddCredentialsEntryPageState extends State<AddCredentialsEntryPage> {
  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final usernameController = TextEditingController();
  final urlController = TextEditingController();
  final passwordController = TextEditingController();
  bool _isObscure = true;

  _AddCredentialsEntryPageState(CredentialsEntry? entry) {
    if (entry != null) {
      _setValues(entry);
    }
  }

  void _setValues(CredentialsEntry entry) async {
    String dbPassword = await SecureStorage.readPassword();
    var passwordEncrypted = await EncryptionUtil.decrypt(
        entry.password,
        dbPassword,
        entry.salt,
        EncryptionUtil.PW_ITERATIONS,
        IV.fromLength(16));

    setState(() {
      titleController.text = entry.title;
      usernameController.text = entry.username;
      urlController.text = entry.url.toString();
      passwordController.text = utf8.decode(passwordEncrypted);
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultBlocConsumer<DatabaseCubit, DatabaseState>(
      builder: (context, snapshot) => _build(context),
      listener: (context, snapshot) {
        if (snapshot is DatabaseSuccessState) {
          Navigator.pop(context);
        }
      },
    );
  }

  Scaffold _build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Entry"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: _buildForm(),
        ),
      ),
      floatingActionButton: buildFab(context),
    );
  }

  Form _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildTitleInput(),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildUserNameInput(),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildUrlInput(),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildPasswordInput(),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 40,
              top: 8.0,
              right: 8.0,
              bottom: 8.0,
            ),
            child: PasswordStrengthIndicator(passwordController.text),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton.icon(
              onPressed: () => _onGeneratePasswordPressed(),
              icon: Icon(Icons.refresh_rounded),
              label: Text("Generate Password"),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildPasswordInput() {
    return DenseTextFormField(
      controller: passwordController,
      obscureText: _isObscure,
      labelText: 'Password',
      icon: Icons.password_outlined,
      suffixIcon: IconButton(
        icon: Icon(_isObscure ? Icons.visibility : Icons.visibility_off),
        onPressed: () => _onToggleVisibility(),
      ),
      onChanged: (asd) {
        setState(() {});
      },
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Password cannot be empty';
        }
        return null;
      },
    );
  }

  Widget _buildUserNameInput() {
    return DenseTextFormField(
      controller: usernameController,
      labelText: 'Username',
      icon: Icons.person_outline_rounded,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Username can not be empty';
        }
        return null;
      },
    );
  }

  Widget _buildUrlInput() {
    return DenseTextFormField(
      controller: usernameController,
      labelText: 'Url',
      icon: Icons.web_rounded,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Username can not be empty';
        }
        return null;
      },
    );
  }

  Widget _buildTitleInput() {
    return DenseTextFormField(
      controller: titleController,
      labelText: 'Title',
      icon: Icons.title,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Title can not be empty';
        }
        return null;
      },
    );
  }

  Widget buildFab(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.save),
      onPressed: () {
        _saveEntry(context);
      },
    );
  }

  void _onGeneratePasswordPressed() {
    setState(() {
      passwordController.text = PasswordGenerator.generatePassword(20);
    });
  }

  void _onToggleVisibility() {
    setState(() {
      _isObscure = !_isObscure;
    });
  }

  _saveEntry(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      context.read<DatabaseCubit>().addCredentials(
          widget.entry != null ? widget.entry!.uuid : Uuid().v4(),
          titleController.text,
          usernameController.text,
          passwordController.text);
    }
  }

  @override
  void dispose() {
    super.dispose();
    titleController.dispose();
    usernameController.dispose();
    passwordController.dispose();
  }
}
