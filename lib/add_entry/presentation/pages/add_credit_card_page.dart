import 'dart:convert';

import 'package:encrypt/encrypt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/presentation/widgets/default_bloc_consumer.dart';
import 'package:kee_pass_manager/common/presentation/widgets/dense_text_fom_field.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credit_card_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/use_cases/database_state.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/utils/card_number_input_formatter.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';
import 'package:kee_pass_manager/utils/string_utils.dart';
import 'package:uuid/uuid.dart';

class AddCreditCardPage extends StatefulWidget {
  final CreditCardEntry? entry;

  AddCreditCardPage([this.entry]);

  @override
  _AddCreditCardPageState createState() => _AddCreditCardPageState(entry);
}

class _AddCreditCardPageState extends State<AddCreditCardPage> {
  final _formKey = GlobalKey<FormState>();

  final titleController = TextEditingController();
  final cardNoController = TextEditingController();
  final pinController = TextEditingController();
  bool _isObscure = true;

  _AddCreditCardPageState(CreditCardEntry? entry) {
    if (entry != null) {
      _setValues(entry);
    }
  }

  void _setValues(CreditCardEntry entry) async {
    String password = await SecureStorage.readPassword();
    var pinEncrypted = utf8.decode(await EncryptionUtil.decrypt(entry.pin,
        password, entry.salt, EncryptionUtil.PW_ITERATIONS, IV.fromLength(16)));

    setState(() {
      titleController.text = entry.title;
      cardNoController.text =
          StringUtils.formatCreditCardNo(entry.cardNo.toString());
      pinController.text = pinEncrypted;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultBlocConsumer<DatabaseCubit, DatabaseState>(
      builder: (context, snapshot) => _build(context),
      listener: (context, snapshot) {
        if (snapshot is DatabaseSuccessState) {
          Navigator.pop(context);
        }
      },
    );
  }

  Scaffold _build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add credit card"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: _buildForm(),
        ),
      ),
      floatingActionButton: _buildFab(context),
    );
  }

  Form _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildTitleInput(),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildCardNoInput(),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildPinInput(),
          ),
        ],
      ),
    );
  }

  Widget _buildTitleInput() {
    return DenseTextFormField(
      controller: titleController,
      labelText: 'Title',
      icon: Icons.title,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Title can not be empty';
        }
        return null;
      },
    );
  }

  Widget _buildCardNoInput() {
    return DenseTextFormField(
      controller: cardNoController,
      labelText: 'Card number',
      icon: Icons.credit_card,
      inputFormatters: [
        FilteringTextInputFormatter.digitsOnly,
        LengthLimitingTextInputFormatter(16),
        CardNumberInputFormatter()
      ],
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Card number can not be empty';
        }
        return null;
      },
    );
  }

  Widget _buildPinInput() {
    return DenseTextFormField(
      controller: pinController,
      obscureText: _isObscure,
      labelText: 'Pin',
      icon: Icons.password_outlined,
      suffixIcon: IconButton(
        icon: Icon(_isObscure ? Icons.visibility : Icons.visibility_off),
        onPressed: () {
          setState(() {
            _isObscure = !_isObscure;
          });
        },
      ),
      onChanged: (asd) {
        setState(() {});
      },
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Pin cannot be empty';
        }
        return null;
      },
    );
  }

  Widget _buildFab(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.save),
      onPressed: () {
        _saveEntry(context);
      },
    );
  }

  Future<void> _saveEntry(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      context.read<DatabaseCubit>().addCreditCard(
          widget.entry != null ? widget.entry!.uuid : Uuid().v4(),
          titleController.text,
          cardNoController.text.replaceAll(" ", ""),
          pinController.text);
    }
  }

  @override
  void dispose() {
    super.dispose();
    titleController.dispose();
    cardNoController.dispose();
    pinController.dispose();
  }
}
