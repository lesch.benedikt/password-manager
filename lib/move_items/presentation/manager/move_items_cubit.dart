import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/repositories/abstract_database_repository.dart';
import 'package:kee_pass_manager/move_items/domain/use_cases/move_item_states.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class MoveItemsCubit extends Cubit<MoveItemState> {
  MoveItemsCubit() : super(MoveItemInitialState()) {
    loadGroups();
  }

  loadGroups() async {
    emit(MoveItemLoadingState());

    String password = await SecureStorage.readPassword();
    var db = await GetIt.I<AbstractDatabaseRepository>().loadDatabase(password);

    emit(MoveItemSuccessState(db: db, selectedGroup: db.root));
  }

  void selectGroup(GroupEntry entry) {
    var successState = state as MoveItemSuccessState;
    emit(MoveItemLoadingState());

    emit(successState.copyWith(selectedGroup: entry));
  }

  void moveItems(List<Entry> selectedItems) async {
    var successState = state as MoveItemSuccessState;
    emit(MoveItemLoadingState());

    String password = await SecureStorage.readPassword();
    var db = await GetIt.I<AbstractDatabaseRepository>().loadDatabase(password);

    var selectedGroup = successState.selectedGroup;

    selectedItems.forEach((element) {
      db.removeEntry(element);

      var newParent = db.findGroup(selectedGroup.uuid);
      if (newParent != null) {
        element.parent = newParent.uuid;
        newParent.entries.add(element);
      }
    });

    GetIt.I<AbstractDatabaseRepository>().saveDatabase(db, password);

    emit(successState.copyWith());
  }

  bool selectParent() {
    var successState = state as MoveItemSuccessState;
    emit(MoveItemLoadingState());

    var parent = successState.selectedGroup.parent;
    if (parent != null) {
      var selectedGroup = successState.db.findGroup(parent);
      emit(successState.copyWith(selectedGroup: selectedGroup));
      return false;
    } else {
      return true;
    }
  }
}
