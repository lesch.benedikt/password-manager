import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/presentation/widgets/default_bloc_builder.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/use_cases/database_state.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/entry_list/presentation/widgets/breadcrumbs.dart';
import 'package:kee_pass_manager/move_items/domain/use_cases/move_item_states.dart';
import 'package:kee_pass_manager/move_items/presentation/manager/move_items_cubit.dart';

class MoveItemsPage extends StatelessWidget {
  final List<Entry> selectedItems;

  MoveItemsPage(this.selectedItems);

  @override
  Widget build(BuildContext context) {
    return DefaultBlocBuilder<MoveItemsCubit, MoveItemState>(
      builder: (context, state) {
        if (state is MoveItemSuccessState) {
          return _build(context, state);
        } else {
          return Container();
        }
      },
    );
  }

  Widget _build(BuildContext context, MoveItemSuccessState state) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: Scaffold(
        appBar: AppBar(
          title: Text("Select group"),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Breadcrumbs(state.db, state.selectedGroup),
            ListView.builder(
              shrinkWrap: true,
              itemCount: state.getGroups().length,
              itemBuilder: (context, pos) {
                return ListTile(
                  leading: Icon(Icons.folder_open_rounded),
                  title: Text(state.getGroups()[pos].title),
                  onTap: () {
                    context
                        .read<MoveItemsCubit>()
                        .selectGroup(state.getGroups()[pos]);
                  },
                );
              },
            ),
          ],
        ),
        bottomNavigationBar: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            TextButton(
              child: Text("Cancel"),
              onPressed: () {
                context.read<DatabaseCubit>().clearSelection();
                Navigator.pop(context);
              },
            ),
            TextButton(
              child: Text("Insert here"),
              onPressed: _isMoveAllowed(context, state)
                  ? () {
                      context.read<MoveItemsCubit>().moveItems(selectedItems);
                    }
                  : null,
            )
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed(BuildContext context) async {
    return context.read<MoveItemsCubit>().selectParent();
  }

  bool _isMoveAllowed(BuildContext context, MoveItemSuccessState state) {
    var sameParent = state.selectedGroup.uuid == selectedItems.first.parent;
    var isSubgroupOfSelf = selectedItems.any((element) =>
        element.isGroup &&
        _isParent(context, element.uuid, state.selectedGroup));
    return !sameParent && !isSubgroupOfSelf;
  }

  bool _isParent(
      BuildContext context, String movedParentUuid, GroupEntry selectedGroup) {
    var db = (context.read<DatabaseCubit>().state as DatabaseSuccessState).db;
    String? currentParent = selectedGroup.uuid;
    do {
      if (currentParent != null) {
        if (currentParent == movedParentUuid) return true;

        var group = db.findGroup(currentParent);
        currentParent = group?.parent;
      }
    } while (currentParent != null);

    return false;
  }
}
