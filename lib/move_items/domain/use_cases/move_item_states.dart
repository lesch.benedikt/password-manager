import 'package:kee_pass_manager/common/domain/use_cases/loading_state.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';

class MoveItemState {}

class MoveItemInitialState extends MoveItemState {}

class MoveItemLoadingState extends MoveItemState implements LoadingState {}

class MoveItemSuccessState extends MoveItemState {
  Database db;
  GroupEntry selectedGroup;

  MoveItemSuccessState({
    required this.db,
    GroupEntry? selectedGroup,
  }) : this.selectedGroup = selectedGroup != null ? selectedGroup : db.root;

  MoveItemSuccessState copyWith({
    Database? db,
    List<GroupEntry>? rootGroups,
    GroupEntry? selectedGroup,
  }) {
    return MoveItemSuccessState(
      db: db ?? this.db,
      selectedGroup: selectedGroup ?? this.selectedGroup,
    );
  }

  List<GroupEntry> getGroups() {
    return selectedGroup.entries
        .where((element) => element.isGroup)
        .map((e) => e as GroupEntry)
        .toList();
  }
}
