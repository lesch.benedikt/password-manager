import 'package:kee_pass_manager/common/domain/use_cases/loading_state.dart';

abstract class InitState {}

class InitLoadingState extends InitState implements LoadingState {}

class InitEnterPasswordState extends InitState implements LoadingState {}

class InitBiometricState extends InitState implements LoadingState {}

class InitNoDataState extends InitState implements LoadingState {}

class InitSuccessState extends InitState implements LoadingState {}
