import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/init_page/domain/use_cases/init_state.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';
import 'package:local_auth/local_auth.dart';

class InitCubit extends Cubit<InitState> {
  InitCubit() : super(InitLoadingState()) {
    load();
  }

  load() async {
    var path = await SecureStorage.readPath();
    bool autoLogin = await SecureStorage.readAutoLogin();

    if (path != null && autoLogin) {
      bool canCheckBiometrics = await LocalAuthentication().canCheckBiometrics;
      bool biometricAuth = await SecureStorage.readBiometricAuth();
      if (canCheckBiometrics && biometricAuth) {
        emit(InitBiometricState());
      } else {
        emit(InitSuccessState());
      }
    } else if (path != null) {
      emit(InitEnterPasswordState());
    } else {
      emit(InitNoDataState());
    }
  }
}
