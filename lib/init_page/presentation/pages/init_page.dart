import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/presentation/widgets/app_icon.dart';
import 'package:kee_pass_manager/create_database/presentation/pages/create_database_page.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/entry_list/presentation/pages/entry_list_page.dart';
import 'package:kee_pass_manager/init_page/domain/use_cases/init_state.dart';
import 'package:kee_pass_manager/init_page/presentation/manager/init_cubit.dart';
import 'package:kee_pass_manager/init_page/presentation/pages/enter_password_page.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';
import 'package:local_auth/local_auth.dart';

class InitPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<InitCubit, InitState>(
      listener: (context, state) async {
        if (state is InitBiometricState) {
          var localAuth = LocalAuthentication();
          bool didAuthenticate = await localAuth.authenticate(
              localizedReason: 'Please authenticate',
              options: const AuthenticationOptions(biometricOnly: true));

          if (didAuthenticate) {
            var password = await SecureStorage.readPassword();
            context.read<DatabaseCubit>().loadEntries(password);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => EntryListPage()),
            );
          }
        } else if (state is InitEnterPasswordState) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EnterPasswordPage()),
          );
        } else if (state is InitNoDataState) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CreateDatabasePage()),
          );
        } else {
          var password = await SecureStorage.readPassword();
          context.read<DatabaseCubit>().loadEntries(password);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EntryListPage()),
          );
        }
      },
      child: _build(context),
    );
  }

  Widget _build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppIcon(),
            Padding(
              padding: const EdgeInsets.all(24.0),
              child: CircularProgressIndicator(),
            ),
            Text(
              "Loading Database",
              style: Theme.of(context).textTheme.titleMedium,
            )
          ],
        ),
      ),
    );
  }
}
