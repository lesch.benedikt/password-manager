import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/presentation/widgets/app_icon.dart';
import 'package:kee_pass_manager/entry_list/domain/use_cases/database_state.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/entry_list/presentation/pages/entry_list_page.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class EnterPasswordPage extends StatefulWidget {
  @override
  State<EnterPasswordPage> createState() => _EnterPasswordPageState();
}

class _EnterPasswordPageState extends State<EnterPasswordPage> {
  final _formKey = GlobalKey<FormState>();
  final passwordController = TextEditingController();
  bool _isObscure = true;
  bool wrongPassword = false;
  bool savePassword = false;

  @override
  Widget build(BuildContext context) {
    return BlocListener<DatabaseCubit, DatabaseState>(
      listener: (context, state) {
        if (state is DatabaseErrorState) {
          setState(() {
            wrongPassword = true;
          });
        } else if (state is DatabaseSuccessState) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EntryListPage()),
          );
        }
      },
      child: _build(context),
    );
  }

  Scaffold _build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Unlock Database"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: AppIcon(),
              ),
              Form(
                key: _formKey,
                child: _buildPasswordInput(),
              ),
              SwitchListTile(
                  title: Text("Auto login"),
                  value: savePassword,
                  onChanged: (value) {
                    setState(() {
                      savePassword = value;
                    });
                  })
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          if (_formKey.currentState?.validate() == true) {
            SecureStorage.savePassword(passwordController.text);
            context.read<DatabaseCubit>().loadEntries(passwordController.text);
          }
        },
        child: Icon(Icons.chevron_right),
      ),
    );
  }

  Widget _buildPasswordInput() {
    return TextFormField(
      controller: passwordController,
      obscureText: _isObscure,
      onChanged: (text) {
        setState(() {
          wrongPassword = false;
        });
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: "Password",
        errorText: wrongPassword ? "Wrong password" : null,
        icon: Icon(Icons.password_outlined),
        isDense: true,
        suffixIcon: IconButton(
          icon: Icon(_isObscure ? Icons.visibility : Icons.visibility_off),
          onPressed: () {
            setState(() {
              _isObscure = !_isObscure;
            });
          },
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Password cannot be empty';
        }
        return null;
      },
    );
  }
}
