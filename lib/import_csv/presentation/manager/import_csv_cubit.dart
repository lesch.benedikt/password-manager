import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:kee_pass_manager/entry_list/domain/repositories/abstract_database_repository.dart';
import 'package:kee_pass_manager/import_csv/domain/repositories/abstract_csv_repository.dart';
import 'package:kee_pass_manager/import_csv/domain/use_cases/import_csv_states.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class ImportCsvCubit extends Cubit<ImportCsvState> {
  ImportCsvCubit() : super(ImportCsvInitialState());

  import(String path) async {
    emit(ImportCsvLoadingState());

    var entries = await GetIt.I<AbstractCsvRepository>().loadCsv(path);

    String dbPassword = await SecureStorage.readPassword();
    var repository = GetIt.I<AbstractDatabaseRepository>();
    var db = await repository.loadDatabase(dbPassword);
    db.addEntries(entries);
    repository.saveDatabase(db, dbPassword);

    emit(ImportCsvSuccessState());
  }
}
