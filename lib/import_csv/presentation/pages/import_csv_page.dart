import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/presentation/widgets/default_bloc_consumer.dart';
import 'package:kee_pass_manager/common/presentation/widgets/dense_text_fom_field.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/import_csv/domain/use_cases/import_csv_states.dart';
import 'package:kee_pass_manager/import_csv/presentation/manager/import_csv_cubit.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class ImportCsvPage extends StatefulWidget {
  @override
  State<ImportCsvPage> createState() => _ImportCsvPageState();
}

class _ImportCsvPageState extends State<ImportCsvPage> {
  final _formKey = GlobalKey<FormState>();
  late TextEditingController csvPathController;

  @override
  Widget build(BuildContext context) {
    return DefaultBlocConsumer<ImportCsvCubit, ImportCsvState>(
      builder: (context, snapshot) {
        return _build(context);
      },
      listener: (context, snapshot) {
        if (snapshot is ImportCsvSuccessState) {
          Navigator.pop(context);

          reloadDB();
        }
      },
    );
  }

  void reloadDB() async {
    String dbPassword = await SecureStorage.readPassword();
    context.read<DatabaseCubit>().loadEntries(dbPassword);
  }

  Scaffold _build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Import CSV file")),
      body: _buildBody(context),
      floatingActionButton: _buildFab(context),
    );
  }

  Padding _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: DenseTextFormField(
                controller: csvPathController,
                readOnly: true,
                labelText: 'Select .csv file',
                icon: Icons.table_chart_outlined,
                suffixIcon: IconButton(
                  icon: Icon(Icons.folder_open_outlined),
                  onPressed: () => _onSelectCsvPressed(context),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please select a .csv file';
                  }
                  return null;
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  FloatingActionButton _buildFab(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.upload_outlined),
      onPressed: () async {
        if (_formKey.currentState?.validate() == true) {
          context.read<ImportCsvCubit>().import(csvPathController.text);
        }
      },
    );
  }

  _onSelectCsvPressed(BuildContext context) async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    setState(() {
      csvPathController.text = result?.files.single.path ?? "";
    });
  }

  @override
  void initState() {
    super.initState();
    csvPathController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    csvPathController.dispose();
  }
}
