import 'package:kee_pass_manager/entry_list/domain/entities/credentials_entry.dart';

abstract class AbstractCsvRepository {
  Future<List<CredentialsEntry>> loadCsv(String path);
}
