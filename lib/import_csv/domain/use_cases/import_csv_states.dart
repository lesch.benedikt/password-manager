import 'package:kee_pass_manager/common/domain/use_cases/loading_state.dart';

class ImportCsvState {}

class ImportCsvInitialState extends ImportCsvState {}

class ImportCsvLoadingState extends ImportCsvState implements LoadingState {}

class ImportCsvSuccessState extends ImportCsvState {

  ImportCsvSuccessState();
}
