import 'dart:typed_data';

import 'package:encrypt/encrypt.dart';
import 'package:get_it/get_it.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credentials_entry.dart';
import 'package:kee_pass_manager/import_csv/data/data_sources/csv_data_source.dart';
import 'package:kee_pass_manager/import_csv/domain/repositories/abstract_csv_repository.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:kee_pass_manager/utils/salt.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';
import 'package:uuid/uuid.dart';

class CsvRepository extends AbstractCsvRepository {
  CsvDataSource _dataSource = GetIt.I<CsvDataSource>();

  Future<List<CredentialsEntry>> loadCsv(String path) async {
    List<List<dynamic>> list = await _dataSource.loadCsv(path);

    var headerList = list.first;

    int titleIndex = headerList
        .indexWhere((element) => element == "Account" || element == "name");
    int userNameIndex = headerList.indexWhere(
        (element) => element == "Login Name" || element == "username");
    int passwordIndex = headerList.indexWhere(
        (element) => element == "Password" || element == "password");
    int urlIndex = headerList
        .indexWhere((element) => element == "Web Site" || element == "url");

    String dbPassword = await SecureStorage.readPassword();
    //TODO no password -> exception

    List<CredentialsEntry> entries = [];
    for (int i = 1; i < list.length; i++) {
      var row = list[i];

      String title = row[titleIndex];
      String password = row[passwordIndex];
      String username = row[userNameIndex];

      if (password.isNotEmpty && username.isNotEmpty && title.isNotEmpty) {
        var salt = Salt.generateAsBase64String(64);
        Uint8List passwordEncrypted = await EncryptionUtil.encrypt(password,
            dbPassword, salt, EncryptionUtil.PW_ITERATIONS, IV.fromLength(16));

        entries.add(CredentialsEntry(
          Uuid().v4(),
          title,
          username,
          passwordEncrypted,
          salt,
          row[urlIndex],
          "",
          DateTime.now(),
        ));
      }
    }
    return entries;
  }
}
