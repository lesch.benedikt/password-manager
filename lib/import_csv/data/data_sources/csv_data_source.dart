import 'dart:io';

import 'package:csv/csv.dart';
import 'package:csv/csv_settings_autodetection.dart';

class CsvDataSource {
  Future<List<List<dynamic>>> loadCsv(String path) async {
    String data = await File(path).readAsString();

    var d = new FirstOccurrenceSettingsDetector(eols: ['\r\n', '\n']);

    List<List<dynamic>> rowsAsListOfValues =
        CsvToListConverter(csvSettingsDetector: d, shouldParseNumbers: false)
            .convert(data);

    return rowsAsListOfValues;
  }
}
