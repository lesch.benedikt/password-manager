import 'package:kee_pass_manager/common/domain/use_cases/loading_state.dart';

abstract class CreateDatabaseState {}

class CreateDatabaseInitialState extends CreateDatabaseState {}

class CreateDatabaseLoadingState extends CreateDatabaseState
    implements LoadingState {}

class CreateDatabaseSuccessState extends CreateDatabaseState {}

class InvalidPasswordState extends CreateDatabaseState {
  InvalidPasswordState();
}
