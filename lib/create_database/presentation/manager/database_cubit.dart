import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:kee_pass_manager/create_database/domain/use_cases/create_database_state.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/repositories/abstract_database_repository.dart';
import 'package:kee_pass_manager/import_kdbx/domain/repositories/abstract_kdbx_repository.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';

class CreateDatabaseCubit extends Cubit<CreateDatabaseState> {
  CreateDatabaseCubit() : super(CreateDatabaseInitialState());

  createEmptyDB(String title, String dir, String password) async {
    emit(CreateDatabaseLoadingState());

    String path =
        "${(await getApplicationDocumentsDirectory()).path}/$title.epdb";
    //TODO path selection

    SecureStorage.savePath(path);

    var db =
        Database(path: path, root: GroupEntry(Uuid().v4(), "root", null, []));

    await GetIt.I<AbstractDatabaseRepository>().saveDatabase(db, password);

    emit(CreateDatabaseSuccessState());
  }

  openDB(String dir, String password) async {
    emit(CreateDatabaseLoadingState());

    SecureStorage.savePath(dir);

    var db =
        Database(path: dir, root: GroupEntry(Uuid().v4(), "root", null, []));

    await GetIt.I<AbstractDatabaseRepository>().saveDatabase(db, password);

    emit(CreateDatabaseSuccessState());
  }

  createFromKdbx(String kdbxPath, String kdbxPassword, String title, String dir,
      String password) async {
    emit(CreateDatabaseLoadingState());

    String dbPassword = await SecureStorage.readPassword();
    var kdbxRoot = await GetIt.I<AbstractKdbxRepository>()
        .loadEntries(kdbxPath, kdbxPassword, dbPassword);

    String path =
        "${(await getApplicationDocumentsDirectory()).path}/$title.epdb";
    //TODO path selection

    SecureStorage.savePath(path);

    var rootGroup = GroupEntry(Uuid().v4(), "root", null, []);
    var db = Database(path: path, root: rootGroup);

    db.addEntries(kdbxRoot.entries);

    await GetIt.I<AbstractDatabaseRepository>().saveDatabase(db, password);

    emit(CreateDatabaseSuccessState());
  }
}
