import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:kee_pass_manager/common/presentation/widgets/default_bloc_consumer.dart';
import 'package:kee_pass_manager/create_database/domain/use_cases/create_database_state.dart';
import 'package:kee_pass_manager/create_database/presentation/manager/database_cubit.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/entry_list/presentation/pages/entry_list_page.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class OpenDatabasePage extends StatefulWidget {
  final storage = new FlutterSecureStorage();

  @override
  _OpenDatabasePageState createState() => _OpenDatabasePageState();
}

class _OpenDatabasePageState extends State<OpenDatabasePage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController? pathController;
  TextEditingController? passwordController;
  bool isVisible = false;
  bool autoLogin = false;

  @override
  Widget build(BuildContext context) {
    return DefaultBlocConsumer<CreateDatabaseCubit, CreateDatabaseState>(
      builder: (context, snapshot) {
        return _build(context);
      },
      listener: (context, snapshot) {
        if (snapshot is CreateDatabaseSuccessState) {
          context
              .read<DatabaseCubit>()
              .loadEntries(passwordController?.text ?? "");
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EntryListPage()),
          );
        }
      },
    );
  }

  Scaffold _build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Open database")),
      body: _buildBody(context),
      floatingActionButton: _buildFab(context),
    );
  }

  Padding _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: _buildForm(context),
    );
  }

  Form _buildForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              controller: pathController,
              readOnly: true,
              onTap: () {
                _onSelectFilePressed(context);
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Save location',
                icon: Icon(Icons.folder_open_outlined),
                isDense: true,
                suffixIcon: IconButton(
                  icon: Icon(Icons.folder_open_outlined),
                  onPressed: () => _onSelectFilePressed(context),
                ),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please select a directory';
                }
                return null;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildPasswordInput(),
          ),
          SwitchListTile(
            title: Text("Auto login"),
            value: autoLogin,
            onChanged: (value) {
              setState(() {
                autoLogin = value;
              });
            },
          ),
        ],
      ),
    );
  }

  TextFormField _buildPasswordInput() {
    return TextFormField(
      onChanged: (password) {
        setState(() {});
      },
      controller: passwordController,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Password',
        icon: Icon(Icons.vpn_key_outlined),
        isDense: true,
        suffixIcon: IconButton(
          icon: Icon(isVisible
              ? Icons.visibility_outlined
              : Icons.visibility_off_outlined),
          onPressed: () {
            setState(() {
              isVisible = !isVisible;
            });
          },
        ),
      ),
      obscureText: !isVisible,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter a password';
        }
        return null;
      },
    );
  }

  FloatingActionButton _buildFab(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.save_rounded),
      onPressed: () async {
        if (_formKey.currentState?.validate() == true) {
          if (autoLogin) {
            SecureStorage.saveAutoLogin(true);
          }
          SecureStorage.savePassword(passwordController?.text ?? "");

          context.read<CreateDatabaseCubit>().openDB(
                pathController?.text ?? "",
                passwordController?.text ?? "",
              );
        }
      },
    );
  }

  _onSelectFilePressed(BuildContext context) async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    setState(() {
      pathController?.text = result?.paths.first ?? "";
    });
  }

  @override
  void initState() {
    super.initState();
    pathController = TextEditingController();
    passwordController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    pathController?.dispose();
    passwordController?.dispose();
  }
}
