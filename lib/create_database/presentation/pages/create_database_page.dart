import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/presentation/widgets/app_icon.dart';
import 'package:kee_pass_manager/create_database/presentation/manager/database_cubit.dart';
import 'package:kee_pass_manager/create_database/presentation/pages/create_empty_db_page.dart';
import 'package:kee_pass_manager/create_database/presentation/pages/create_from_kdbx_page.dart';
import 'package:kee_pass_manager/create_database/presentation/pages/open_database_page.dart';

class CreateDatabasePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Create Database")),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: AppIcon(),
            ),
            OutlinedButton(
              child: Text("Create empty database"),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return BlocProvider(
                      create: (BuildContext context) => CreateDatabaseCubit(),
                      child: CreateEmptyDbPage(),
                    );
                  }),
                );
              },
            ),
            OutlinedButton(
              child: Text("Open existing database"),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return BlocProvider(
                      create: (BuildContext context) => CreateDatabaseCubit(),
                      child: OpenDatabasePage(),
                    );
                  }),
                );
              },
            ),
            OutlinedButton(
              child: Text("Create from KeePass"),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return BlocProvider(
                      create: (BuildContext context) => CreateDatabaseCubit(),
                      child: CreateFromKdbxPage(),
                    );
                  }),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
