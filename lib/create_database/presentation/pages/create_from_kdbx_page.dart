import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:kee_pass_manager/add_entry/presentation/widgets/password_strength_indicator.dart';
import 'package:kee_pass_manager/common/presentation/widgets/default_bloc_consumer.dart';
import 'package:kee_pass_manager/create_database/domain/use_cases/create_database_state.dart';
import 'package:kee_pass_manager/create_database/presentation/manager/database_cubit.dart';
import 'package:kee_pass_manager/entry_list/presentation/pages/entry_list_page.dart';
import 'package:kee_pass_manager/utils/password_generator.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class CreateFromKdbxPage extends StatefulWidget {
  final storage = new FlutterSecureStorage();

  @override
  _CreateFromKdbxPageState createState() => _CreateFromKdbxPageState();
}

class _CreateFromKdbxPageState extends State<CreateFromKdbxPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController? kdbxPathController;
  TextEditingController? kdbxPasswordController;
  TextEditingController? titleController;
  TextEditingController? dbPathController;
  TextEditingController? dbPasswordController;
  bool autoLogin = false;

  @override
  Widget build(BuildContext context) {
    return DefaultBlocConsumer<CreateDatabaseCubit, CreateDatabaseState>(
      builder: (context, snapshot) => _build(context),
      listener: (context, snapshot) {
        if (snapshot is CreateDatabaseSuccessState) {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EntryListPage()),
          );
        }
      },
    );
  }

  Scaffold _build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create from KeePass"),
      ),
      body: _buildBody(context),
      floatingActionButton: _buildFab(context),
    );
  }

  Padding _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _buildTitleInput(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _buildSavePathInput(context),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _buildPasswordInput(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _buildKeypassPathInput(context),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _buildKeypassPasswordInput(),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 40,
                top: 8.0,
                right: 8.0,
                bottom: 8.0,
              ),
              child:
                  PasswordStrengthIndicator(dbPasswordController?.text ?? ""),
            ),
            OutlinedButton(
              onPressed: () {
                dbPasswordController?.text =
                    PasswordGenerator.generatePassword(20);
                setState(() {});
              },
              child: Text("Generate Password"),
            ),
            SwitchListTile(
              title: Text("Auto login"),
              value: autoLogin,
              onChanged: (value) {
                setState(() {
                  autoLogin = value;
                });
              },
            ),
          ],
        ),
      ),
    );
  }

  TextFormField _buildKeypassPasswordInput() {
    return TextFormField(
      onChanged: (value) {
        setState(() {});
      },
      controller: dbPasswordController,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Password',
        icon: Icon(Icons.vpn_key_outlined),
        isDense: true,
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter password';
        }
        return null;
      },
    );
  }

  TextFormField _buildKeypassPathInput(BuildContext context) {
    return TextFormField(
      controller: dbPathController,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Directory',
        icon: Icon(Icons.folder_open_outlined),
        isDense: true,
        suffixIcon: IconButton(
          icon: Icon(Icons.folder_open_outlined),
          onPressed: () => _onSelectSavePathPressed(context),
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'PLease select a directory';
        }
        return null;
      },
    );
  }

  TextFormField _buildTitleInput() {
    return TextFormField(
      controller: titleController,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Title',
        icon: Icon(Icons.title),
        isDense: true,
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'PLease enter a title';
        }
        return null;
      },
    );
  }

  TextFormField _buildPasswordInput() {
    return TextFormField(
      controller: kdbxPasswordController,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Password',
        icon: Icon(Icons.vpn_key_outlined),
        isDense: true,
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'PLease enter a password';
        }
        return null;
      },
    );
  }

  TextFormField _buildSavePathInput(BuildContext context) {
    return TextFormField(
      controller: kdbxPathController,
      readOnly: true,
      decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Database',
          icon: Icon(Icons.table_chart_outlined),
          isDense: true,
          suffixIcon: IconButton(
            icon: Icon(Icons.folder_open_outlined),
            onPressed: () => _onSelectKdbxDBPressed(context),
          )),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please select a database';
        }
        return null;
      },
    );
  }

  FloatingActionButton _buildFab(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.navigate_next),
      onPressed: () async {
        if (_formKey.currentState?.validate() == true) {
          if (autoLogin) {
            SecureStorage.saveAutoLogin(true);
          }
          SecureStorage.savePassword(dbPasswordController?.text ?? "");

          context.read<CreateDatabaseCubit>().createFromKdbx(
              kdbxPathController?.text ?? "",
              kdbxPasswordController?.text ?? "",
              titleController?.text ?? "",
              dbPathController?.text ?? "",
              dbPasswordController?.text ?? "");
        }
      },
    );
  }

  _onSelectKdbxDBPressed(BuildContext context) async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    setState(() {
      kdbxPathController?.text = result?.files.single.path ?? "";
    });
  }

  _onSelectSavePathPressed(BuildContext context) async {
    String? result = await FilePicker.platform.getDirectoryPath();

    setState(() {
      dbPathController?.text = result ?? "";
    });
  }

  @override
  void initState() {
    super.initState();
    kdbxPasswordController = TextEditingController();
    kdbxPathController = TextEditingController();
    titleController = TextEditingController();
    dbPathController = TextEditingController();
    dbPasswordController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    kdbxPasswordController?.dispose();
    kdbxPathController?.dispose();
    titleController?.dispose();
    dbPathController?.dispose();
    dbPasswordController?.dispose();
  }
}
