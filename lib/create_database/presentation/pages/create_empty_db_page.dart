import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:kee_pass_manager/add_entry/presentation/widgets/password_strength_indicator.dart';
import 'package:kee_pass_manager/common/presentation/widgets/default_bloc_consumer.dart';
import 'package:kee_pass_manager/create_database/domain/use_cases/create_database_state.dart';
import 'package:kee_pass_manager/create_database/presentation/manager/database_cubit.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/entry_list/presentation/pages/entry_list_page.dart';
import 'package:kee_pass_manager/utils/password_generator.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class CreateEmptyDbPage extends StatefulWidget {
  final storage = new FlutterSecureStorage();

  @override
  _CreateEmptyDbPageState createState() => _CreateEmptyDbPageState();
}

class _CreateEmptyDbPageState extends State<CreateEmptyDbPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController? titleController;
  TextEditingController? pathController;
  TextEditingController? passwordController;
  bool isVisible = false;
  bool autoLogin = false;

  @override
  Widget build(BuildContext context) {
    return DefaultBlocConsumer<CreateDatabaseCubit, CreateDatabaseState>(
      builder: (context, snapshot) {
        return _build(context);
      },
      listener: (context, snapshot) {
        if (snapshot is CreateDatabaseSuccessState) {
          context
              .read<DatabaseCubit>()
              .loadEntries(passwordController?.text ?? "");
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EntryListPage()),
          );
        }
      },
    );
  }

  Scaffold _build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Create empty database")),
      body: _buildBody(context),
      floatingActionButton: _buildFab(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: _buildForm(context),
      ),
    );
  }

  Form _buildForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildTitleInput(),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildSaveLocationInput(context),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildPasswordInput(),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 40, top: 8.0, right: 8.0, bottom: 8.0),
            child: PasswordStrengthIndicator(passwordController?.text ?? ""),
          ),
          OutlinedButton(
            onPressed: () {
              passwordController?.text = PasswordGenerator.generatePassword(20);
              setState(() {});
            },
            child: Text("Generate Password"),
          ),
          SwitchListTile(
            title: Text("Auto login"),
            value: autoLogin,
            onChanged: (value) {
              setState(() {
                autoLogin = value;
              });
            },
          ),
        ],
      ),
    );
  }

  TextFormField _buildTitleInput() {
    return TextFormField(
      controller: titleController,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Database name',
        icon: Icon(Icons.title),
        isDense: true,
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter a name';
        }
        return null;
      },
    );
  }

  TextFormField _buildSaveLocationInput(BuildContext context) {
    return TextFormField(
      controller: pathController,
      readOnly: true,
      onTap: () => _onSelectFilePressed(context),
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Save location',
        icon: Icon(Icons.folder_open_outlined),
        isDense: true,
        suffixIcon: IconButton(
          icon: Icon(Icons.folder_open_outlined),
          onPressed: () => _onSelectFilePressed(context),
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please select a directory';
        }
        return null;
      },
    );
  }

  TextFormField _buildPasswordInput() {
    return TextFormField(
      controller: passwordController,
      obscureText: !isVisible,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Password',
        icon: Icon(Icons.vpn_key_outlined),
        isDense: true,
        suffixIcon: _buildToggleVisibilityButton(),
      ),
      onChanged: (password) {
        setState(() {});
      },
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter a password';
        }
        return null;
      },
    );
  }

  IconButton _buildToggleVisibilityButton() {
    return IconButton(
      icon: Icon(isVisible
          ? Icons.visibility_outlined
          : Icons.visibility_off_outlined),
      onPressed: () {
        setState(() {
          isVisible = !isVisible;
        });
      },
    );
  }

  FloatingActionButton _buildFab(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.save_rounded),
      onPressed: () async {
        if (_formKey.currentState?.validate() == true) {
          if (autoLogin) {
            SecureStorage.saveAutoLogin(true);
          }
          SecureStorage.savePassword(passwordController?.text ?? "");

          context.read<CreateDatabaseCubit>().createEmptyDB(
                titleController?.text ?? "",
                pathController?.text ?? "",
                passwordController?.text ?? "",
              );
        }
      },
    );
  }

  _onSelectFilePressed(BuildContext context) async {
    String? result = await FilePicker.platform.getDirectoryPath();

    setState(() {
      pathController?.text = result ?? "";
    });
  }

  @override
  void initState() {
    super.initState();
    titleController = TextEditingController();
    pathController = TextEditingController();
    passwordController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    titleController?.dispose();
    pathController?.dispose();
    passwordController?.dispose();
  }
}
