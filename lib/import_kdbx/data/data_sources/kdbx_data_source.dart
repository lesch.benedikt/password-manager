import 'dart:io';
import 'dart:typed_data';

import 'package:kdbx/kdbx.dart';

class KdbxDataSource {
  Future<KdbxFile> loadKdbxFile(String path, String password) async {
    final data = Uint8List.view((await File(path).readAsBytes()).buffer);

    var kdbxFormat = KdbxFormat();
    final file = await kdbxFormat.read(
        data, Credentials(ProtectedValue.fromString(password)));

    return file;
  }
}
