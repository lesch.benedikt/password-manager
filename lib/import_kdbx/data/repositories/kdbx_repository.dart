import 'package:encrypt/encrypt.dart';
import 'package:get_it/get_it.dart';
import 'package:kdbx/kdbx.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credentials_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';
import 'package:kee_pass_manager/import_kdbx/data/data_sources/kdbx_data_source.dart';
import 'package:kee_pass_manager/import_kdbx/domain/repositories/abstract_kdbx_repository.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:kee_pass_manager/utils/salt.dart';

class KdbxRepository extends AbstractKdbxRepository {
  KdbxDataSource _dataSource = GetIt.I<KdbxDataSource>();

  Future<GroupEntry> loadEntries(
      String path, String password, String dbPassword) async {
    KdbxFile file = await _dataSource.loadKdbxFile(path, password);

    var rootID = file.body.rootGroup.uuid.uuid;
    GroupEntry entry = GroupEntry(rootID, "root", null, []);
    List<Entry> list =
        await _getEntries(file.body.rootGroup, entry, dbPassword);
    entry.entries = list;

    return entry;
  }

  Future<List<Entry>> _getEntries(
      KdbxGroup kdbxGroup, GroupEntry parent, String dbPassword) async {
    List<Entry> list = [];
    List<Entry> groups = [];

    for (KdbxGroup element in kdbxGroup.groups) {
      var name = element.name.get().toString();
      var group = GroupEntry(element.uuid.uuid, name, parent.uuid, []);
      group.entries = await _getEntries(element, group, dbPassword);
      groups.add(group);
    }

    list.addAll(groups);

    List<Entry> entries = [];

    for (KdbxEntry element in kdbxGroup.entries) {
      CredentialsEntry newEntry =
          await _createEntry(element, dbPassword, parent);

      entries.add(newEntry);
    }

    list.addAll(entries);

    return list;
  }

  Future<CredentialsEntry> _createEntry(
      KdbxEntry element, String dbPassword, GroupEntry parent) async {
    var salt = Salt.generateAsBase64String(64);
    var passwordEncrypted = await EncryptionUtil.encrypt(
        _getString(element, "Password"),
        dbPassword,
        salt,
        EncryptionUtil.PW_ITERATIONS,
        IV.fromLength(16));

    var times = element.times;
    var newEntry = CredentialsEntry(
        element.uuid.uuid,
        _getString(element, "Title"),
        _getString(element, "UserName"),
        passwordEncrypted,
        salt,
        Uri.parse(_getString(element, "Url")),
        _getString(element, "Notes"),
        _convertDateTime(times.creationTime.get()),
        expireTime: times.expires.get() == true ? times.expiryTime.get() : null,
        parent: parent.uuid);
    return newEntry;
  }

  String _getString(KdbxEntry element, String keyName) {
    return element.getString(KdbxKey(keyName))?.getText() ?? "";
  }

  DateTime _convertDateTime(DateTime? node) {
    return node ?? DateTime.now();
  }
}
