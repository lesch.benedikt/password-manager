import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/presentation/widgets/default_bloc_consumer.dart';
import 'package:kee_pass_manager/import_kdbx/domain/use_cases/import_kdbx_states.dart';
import 'package:kee_pass_manager/import_kdbx/presentation/manager/import_kdbx_cubit.dart';

class ImportKdbxPage extends StatefulWidget {
  @override
  State<ImportKdbxPage> createState() => _ImportKdbxPageState();
}

class _ImportKdbxPageState extends State<ImportKdbxPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController? kdbxPathController;
  TextEditingController? passwordController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Import KeePass database")),
      body: DefaultBlocConsumer<ImportKdbxCubit, ImportKdbxState>(
        listener: (context, state) {
          if (state is ImportKdbxSuccessState) {
            Navigator.pop(context);
          }
        },
        builder: (context, snapshot) {
          return _buildBody(context);
        },
      ),
      floatingActionButton: _buildFab(context),
    );
  }

  Padding _buildBody(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: kdbxPathController,
                readOnly: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Database location',
                    icon: Icon(Icons.table_chart_outlined),
                    isDense: true,
                    suffixIcon: IconButton(
                      icon: Icon(Icons.folder_open_outlined),
                      onPressed: () => _onSelectKdbxDBPressed(context),
                    )),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please select a database';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: passwordController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                  icon: Icon(Icons.vpn_key_outlined),
                  isDense: true,
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'PLease enter a password';
                  }
                  return null;
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  FloatingActionButton _buildFab(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.upload_outlined),
      onPressed: () async {
        if (_formKey.currentState?.validate() == true) {
          context.read<ImportKdbxCubit>().importKdbx(
              kdbxPathController?.text ?? "", passwordController?.text ?? "");
        }
      },
    );
  }

  _onSelectKdbxDBPressed(BuildContext context) async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    setState(() {
      kdbxPathController?.text = result?.files.single.path ?? "";
    });
  }

  @override
  void initState() {
    super.initState();
    passwordController = TextEditingController();
    kdbxPathController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    passwordController?.dispose();
    kdbxPathController?.dispose();
  }
}
