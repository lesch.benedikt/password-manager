import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:kee_pass_manager/entry_list/domain/repositories/abstract_database_repository.dart';
import 'package:kee_pass_manager/import_kdbx/domain/repositories/abstract_kdbx_repository.dart';
import 'package:kee_pass_manager/import_kdbx/domain/use_cases/import_kdbx_states.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class ImportKdbxCubit extends Cubit<ImportKdbxState> {
  ImportKdbxCubit() : super(ImportKdbxInitialState());

  importKdbx(String kdbxPath, String kdbxPassword) async {
    emit(ImportKdbxLoadingState());

    String password = await SecureStorage.readPassword();

    var kdbxRoot = await GetIt.I<AbstractKdbxRepository>()
        .loadEntries(kdbxPath, kdbxPassword, password);

    var db = await GetIt.I<AbstractDatabaseRepository>().loadDatabase(password);
    db.addEntries(kdbxRoot.entries);

    await GetIt.I<AbstractDatabaseRepository>().saveDatabase(db, password);

    emit(ImportKdbxSuccessState(kdbxRoot));
  }
}
