import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';

abstract class AbstractKdbxRepository {
  Future<GroupEntry> loadEntries(String path, String password, String dbPassword);
}
