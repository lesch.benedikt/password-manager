import 'package:kee_pass_manager/common/domain/use_cases/loading_state.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';

class ImportKdbxState {}

class ImportKdbxInitialState extends ImportKdbxState {}

class ImportKdbxLoadingState extends ImportKdbxState implements LoadingState {}

class ImportKdbxSuccessState extends ImportKdbxState {
  GroupEntry root;

  ImportKdbxSuccessState(this.root);
}
