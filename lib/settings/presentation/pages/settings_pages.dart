import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/presentation/widgets/default_bloc_builder.dart';
import 'package:kee_pass_manager/settings/domain/use_cases/settings_state.dart';
import 'package:kee_pass_manager/settings/presentation/manager/settings_cubit.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class SettingsPage extends StatefulWidget {
  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultBlocBuilder<SettingsCubit, SettingsState>(
      builder: (context, state) {
        if (state is SettingsSuccessState) {
          return _build(context, state);
        } else {
          return Container();
        }
      },
    );
  }

  Scaffold _build(BuildContext context, SettingsSuccessState state) {
    return Scaffold(
      appBar: AppBar(title: Text("Settings")),
      body: Column(
        children: [
          SwitchListTile(
            title: Text("Auto login"),
            value: state.savePassword,
            onChanged: (value) {
              SecureStorage.saveAutoLogin(value);
            },
          ),
          SwitchListTile(
            title: Text("Biometric Login"),
            value: state.biometricLogin,
            onChanged: state.savePassword
                ? (value) {
                    setState(() {
                      context.read<SettingsCubit>().setBiometricLogin(value);
                    });
                  }
                : null,
          ),
        ],
      ),
    );
  }
}
