import 'package:bloc/bloc.dart';
import 'package:kee_pass_manager/settings/domain/use_cases/settings_state.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class SettingsCubit extends Cubit<SettingsState> {
  SettingsCubit() : super(SettingsInitialState()) {
    load();
  }

  load() async {
    emit(SettingsLoadingState());

    var autoLogin = await SecureStorage.readAutoLogin();
    var biometricLogin = await SecureStorage.readBiometricAuth();

    emit(SettingsSuccessState(autoLogin, biometricLogin));
  }

  void setBiometricLogin(bool value) {
    SecureStorage.saveBiometricAuth(value);
    emit(SettingsSuccessState(true, value));
  }
}
