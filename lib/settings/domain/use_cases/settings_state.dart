import 'package:kee_pass_manager/common/domain/use_cases/loading_state.dart';

class SettingsState {}

class SettingsInitialState extends SettingsState {}

class SettingsLoadingState extends SettingsState implements LoadingState {}

class SettingsSuccessState extends SettingsState {
  bool savePassword = false;
  bool biometricLogin = false;

  SettingsSuccessState(this.savePassword, this.biometricLogin);
}
