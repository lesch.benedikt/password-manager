class Account {
  String title;
  String username;
  String password;

  Account(this.title, this.username, this.password);

  Map<String, dynamic> toJson() => {
        'title': title,
        'username': username,
        'password': password,
      };

  @override
  String toString() {
    return 'Account{title: $title, username: $username, password: $password}';
  }
}
