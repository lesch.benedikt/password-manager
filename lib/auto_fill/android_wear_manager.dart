import 'dart:convert';
import 'dart:ui';

import 'package:encrypt/encrypt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kee_pass_manager/auto_fill/account.dart';
import 'package:kee_pass_manager/entry_list/data/data_sources/database_source.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credentials_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:kee_pass_manager/utils/salt.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';
import 'package:uuid/uuid.dart';

const String _backgroundName = 'com.vgmmedia.flutter.expenses/background';

class AndroidAutofillManager {
  static const String _channelName = 'com.vgmmedia.flutter.expenses/wear';
  static final MethodChannel _channel = const MethodChannel(_channelName);

  static void initialize() async {
    print("AndroidAutofillManager.initialize");
    final CallbackHandle? handle =
        PluginUtilities.getCallbackHandle(wearMessageCallbackDispatcher);

    await _channel.invokeMethod<bool>('start', handle?.toRawHandle());
  }
}

void wearMessageCallbackDispatcher() {
  WidgetsFlutterBinding.ensureInitialized();

  const MethodChannel? _channel =
      MethodChannel(_backgroundName, JSONMethodCodec());

  _channel.setMethodCallHandler((MethodCall call) async {
    await _onMethodCalled(call, _channel);
  });

  _channel.invokeMethod<void>('initialized');
}

Future<void> _onMethodCalled(MethodCall call, MethodChannel _channel) async {
  print("Method call ${call.method}");
  if (call.method == "searchAccount") {
    final List<String> args =
        (call.arguments as List<dynamic>).map((e) => e as String).toList();
    var packages = (jsonDecode(args[0]) as List<dynamic>)
        .map((e) => e.toString())
        .toList();
    var domains = (jsonDecode(args[1]) as List<dynamic>)
        .map((e) => e.toString())
        .toList();

    var account = await getAccount(packages, domains);

    print("Retrieved account $account");
    _channel.invokeMethod<void>('passAccount', account);
  } else if (call.method == "saveAccount") {
    final List<String> args =
        (call.arguments as List<dynamic>).map((e) => e as String).toList();

    var login = args[0];
    var password = args[1];
    var packages = (jsonDecode(args[2]) as List<dynamic>)
        .map((e) => e.toString())
        .toList();
    var domains = (jsonDecode(args[3]) as List<dynamic>)
        .map((e) => Uri.parse(e))
        .toList();
    saveAccount(login, password, packages, domains);
  }
}

void saveAccount(String login, String password, List<String> packages,
    List<Uri> domains) async {
  String dbPassword = await SecureStorage.readPassword();

  DatabaseSource source = DatabaseSource();
  Database db = await source.loadDatabase(dbPassword);

  var salt = Salt.generateAsBase64String(64);

  var passwortEncypted = await EncryptionUtil.encrypt(password, dbPassword,
      salt, EncryptionUtil.PW_ITERATIONS, IV.fromLength(16));

  //TODO multi domains?
  db.addEntry(new CredentialsEntry(Uuid().v4(), packages.first, login,
      passwortEncypted, salt, domains.first, packages.join(","), DateTime.now(),
      parent: db.root.uuid));

  source.saveDatabase(db, dbPassword);

  //TODO reload DB?
}

Future<Account?> getAccount(List<String> packages, List<String> domains) async {
  String password = await SecureStorage.readPassword();

//TODO Can't use getit here for some reason
  Database db = await DatabaseSource().loadDatabase(password);

  Entry? entry = searchInGroup(packages, domains, db.root);
  if (entry != null) {
    return entry.getAutofillPair();
  }

  return null;
}

Entry? searchInGroup(
  List<String> packages,
  List<String> domains,
  GroupEntry groupEntry,
) {
  for (Entry element in groupEntry.entries) {
    if (element.isGroup) {
      Entry? entry = searchInGroup(packages, domains, element as GroupEntry);
      if (entry != null) {
        return entry;
      }
    } else {
      if (packages.any((search) => element.matches(search)) ||
          domains.any((search) => element.matches(search))) {
        return element;
      }
    }
  }

  return null;
}
