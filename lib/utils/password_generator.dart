import 'dart:math';

class PasswordGenerator {
  static String generatePassword(int length) {
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890@#!?%§%&€+\$';

    Random _rnd = Random();

    var password;
    do {
      var charCodes = Iterable.generate(length, (_) {
        return _chars.codeUnitAt(_rnd.nextInt(_chars.length));
      });
      password = String.fromCharCodes(charCodes);
    } while (getPasswordStrength(password) < 8);

    return password;
  }

  static int getPasswordStrength(String password){
    int strength = _getLengthStrength(password);

    if (_containsUpperLetter(password)) strength++;
    if (_containsLowerLetter(password)) strength++;
    if (_containsNumber(password)) strength++;
    if (_containsSpecial(password)) strength++;

    return strength;
  }

  static _containsUpperLetter(String password) {
    return password.contains(RegExp("[A-Z]+"));
  }

  static _containsLowerLetter(String password) {
    return password.contains(RegExp("[a-z]+"));
  }

  static _containsNumber(String password) {
    return password.contains(RegExp("[0-9]+"));
  }

  static _containsSpecial(String password) {
    return password.contains(RegExp("[^A-Za-z0-9]+"));
  }

  static _getLengthStrength(String password) {
    if (password.length >= 20) {
      return 6;
    } if (password.length >= 15) {
      return 4;
    } else if (password.length >= 10) {
      return 2;
    }

    return 0;
  }
}
