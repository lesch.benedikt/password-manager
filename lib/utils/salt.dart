import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

class Salt {
  static List<int> generate(int length) {
    var buffer = new Uint8List(length);
    var rng = new Random.secure();
    for (var i = 0; i < length; i++) {
      buffer[i] = rng.nextInt(256);
    }

    return buffer;
  }

  static String generateAsBase64String(int length) {
    var encoder = new Base64Encoder();
    return encoder.convert(generate(length));
  }
}
