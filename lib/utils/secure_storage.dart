import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:kee_pass_manager/common/domain/entities/no_password_exception.dart';

class SecureStorage {
  static const DB_PATH_KEY = "path";
  static const AUTO_LOGIN_KEY = "autologin";
  static const PASSWORD_KEY = "password";
  static const BIOMETRIC_AUTH_KEY = "biometricAuth";

  static savePath(String path) {
    FlutterSecureStorage().write(key: DB_PATH_KEY, value: path);
  }

  static Future<String?> readPath() async {
    if (await FlutterSecureStorage().containsKey(key: DB_PATH_KEY)) {
      var read = await FlutterSecureStorage().read(key: DB_PATH_KEY);
      return read ?? null;
    } else {
      return null;
    }
  }

  static saveAutoLogin(bool autologin) {
    FlutterSecureStorage()
        .write(key: AUTO_LOGIN_KEY, value: autologin ? "1" : "0");
  }

  static Future<bool> readAutoLogin() async {
    if (await FlutterSecureStorage().containsKey(key: AUTO_LOGIN_KEY)) {
      var read = await FlutterSecureStorage().read(key: AUTO_LOGIN_KEY);
      return read == "1";
    } else {
      return false;
    }
  }

  static savePassword(String password) {
    FlutterSecureStorage().write(key: PASSWORD_KEY, value: password);
  }

  static Future<String> readPassword() async {
    if (await FlutterSecureStorage().containsKey(key: PASSWORD_KEY)) {
      var read = await FlutterSecureStorage().read(key: PASSWORD_KEY);
      if (read != null) {
        return read;
      } else {
        throw NoPasswordException();
      }
    } else {
      throw NoPasswordException();
    }
  }

  static saveBiometricAuth(bool value) {
    FlutterSecureStorage()
        .write(key: BIOMETRIC_AUTH_KEY, value: value ? "1" : "0");
  }

  static Future<bool> readBiometricAuth() async {
    if (await FlutterSecureStorage().containsKey(key: BIOMETRIC_AUTH_KEY)) {
      var read = await FlutterSecureStorage().read(key: BIOMETRIC_AUTH_KEY);
      return read == "1";
    } else {
      return false;
    }
  }
}
