class StringUtils {
  static String formatCreditCardNo(String string) {
    return "${string.substring(0, 4)} ${string.substring(4, 8)} ${string.substring(8, 12)} ${string.substring(12, 16)}";
  }
}
