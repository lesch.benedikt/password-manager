import 'dart:convert';
import 'dart:typed_data';

import 'package:cryptography/cryptography.dart';
import 'package:encrypt/encrypt.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/utils/salt.dart';

class EncryptionUtil {
  static const int DB_SALT_LENGTH = 64;
  static const int DB_ITERATIONS = 100000;
  static const int PW_ITERATIONS = 34000;

  static Future<Uint8List> encryptDb(Database database, String password) async {
    int iterations = DB_ITERATIONS;
    Uint8List dbSalt = Uint8List.fromList(Salt.generate(DB_SALT_LENGTH));
    IV iv = IV.fromSecureRandom(16);

    var json = jsonEncode(database.toJson());

    Uint8List encryptedDb = await EncryptionUtil.encrypt(
        json, password, base64.encode(dbSalt), iterations, iv);

    Uint8List iterationsBytes = Uint8List(32)
      ..buffer.asInt32List()[0] = iterations;
    Uint8List saltBytes = Uint8List.fromList(dbSalt);

    return Uint8List.fromList(
        saltBytes + iterationsBytes + iv.bytes + encryptedDb);
  }

  static Future<String> decryptDb(Uint8List readBytes, String password) async {
    Uint8List salt = readBytes.sublist(0, DB_SALT_LENGTH);
    int iterations = readBytes
        .sublist(DB_SALT_LENGTH, DB_SALT_LENGTH + 32)
        .buffer
        .asInt32List()[0];
    IV iv =
        IV(readBytes.sublist(DB_SALT_LENGTH + 32, DB_SALT_LENGTH + 32 + 16));
    Uint8List database = readBytes.sublist(DB_SALT_LENGTH + 32 + 16);

    var decrypted = await EncryptionUtil.decrypt(
        database, password, base64.encode(salt), iterations, iv);
    return utf8.decode(decrypted);
  }

  static Future<Uint8List> encrypt(String message, String password, String salt,
      int iterations, IV iv) async {
    Key key = await generateKeyFromPass(iterations, password, salt);

    final encrypter = Encrypter(AES(key));
    final encrypted = encrypter.encrypt(message, iv: iv);

    return encrypted.bytes;
  }

  static Future<List<int>> decrypt(Uint8List crypto, String password,
      String salt, int iterations, IV iv) async {
    Key key = await generateKeyFromPass(iterations, password, salt);

    final encrypter = Encrypter(AES(key));

    return encrypter.decryptBytes(Encrypted(crypto), iv: iv);
  }

  static Future<Key> generateKeyFromPass(
      int iterations, String password, String salt) async {
    var generator =
        Pbkdf2(macAlgorithm: Hmac.sha256(), iterations: iterations, bits: 192);
    var secretKey = await generator.deriveKey(
        secretKey: SecretKey(password.codeUnits), nonce: salt.codeUnits);

    var extract = await secretKey.extract();
    var base64encode = base64Encode(extract.bytes);
    final key = Key.fromBase64(base64encode);

    return key;
  }
}
