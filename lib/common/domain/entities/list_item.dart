import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class ListItem<T> implements Comparable<ListItem> {
  Key key;
  T item;
  bool isSelected;

  ListItem(this.item, [this.isSelected = false]) : this.key = Key(Uuid().v4());

  @override
  int compareTo(ListItem other) {
    return other.item?.compareTo(this.item) * -1;
  }
}
