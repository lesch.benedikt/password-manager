import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DenseTextFormField extends StatelessWidget {
  final TextEditingController controller;
  final IconData icon;
  final String labelText;
  final FormFieldValidator<String>? validator;
  final bool readOnly;
  final IconButton? suffixIcon;
  final bool obscureText;
  final ValueChanged<String>? onChanged;
  final List<TextInputFormatter>? inputFormatters;

  DenseTextFormField({
    required this.controller,
    required this.icon,
    required this.labelText,
    this.validator,
    this.readOnly = false,
    this.suffixIcon,
    this.obscureText = false,
    this.onChanged,
    this.inputFormatters,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      readOnly: readOnly,
      obscureText: obscureText,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: labelText,
        icon: Icon(icon),
        isDense: true,
        suffixIcon: suffixIcon,
      ),
      onChanged: onChanged,
      validator: validator,
      inputFormatters: inputFormatters,
    );
  }
}
