import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/domain/use_cases/error_state.dart';
import 'package:kee_pass_manager/common/domain/use_cases/loading_state.dart';

class DefaultBlocConsumer<C extends Cubit<S>, S> extends StatelessWidget {
  final Widget Function(BuildContext context, S state) builder;
  final Widget Function(BuildContext context, S state)? errorBuilder;
  final void Function(BuildContext context, S state) listener;

  DefaultBlocConsumer(
      {required this.builder, this.errorBuilder, required this.listener});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<C, S>(
      builder: (context, state) {
        if (state is LoadingState) {
          return Scaffold(
            body: Center(
              child: SizedBox(
                width: 32,
                height: 32,
                child: CircularProgressIndicator(),
              ),
            ),
          );
        } else if (state is ErrorState) {
          var builder = errorBuilder;
          if (builder != null) {
            return builder(context, state);
          } else {
            return Scaffold(
              body: Center(
                child: Text("Error Occurred"), //TODO
              ),
            );
          }
        } else {
          return builder(context, state);
        }
      },
      listener: listener,
    );
  }
}
