import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/domain/use_cases/error_state.dart';
import 'package:kee_pass_manager/common/domain/use_cases/loading_state.dart';

class DefaultBlocBuilder<C extends Cubit<S>, S> extends StatelessWidget {
  final Widget Function(BuildContext context, S state) builder;
  final Widget Function(BuildContext context, S state)? errorBuilder;

  DefaultBlocBuilder({required this.builder, this.errorBuilder});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<C, S>(builder: (context, state) {
      if (state is LoadingState) {
        return Scaffold(
          body: Center(
            child: SizedBox(
              width: 32,
              height: 32,
              child: CircularProgressIndicator(),
            ),
          ),
        );
      } else if (state is ErrorState) {
        var builder = errorBuilder;
        if (builder != null) {
          return builder(context, state);
        } else {
          return Scaffold(
            body: Center(
              child: Text("Error Occurred"), //TODO
            ),
          );
        }
      } else {
        return builder(context, state);
      }
    });
  }
}
