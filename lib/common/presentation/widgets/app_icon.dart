import 'package:flutter/material.dart';

class AppIcon extends StatelessWidget {
  const AppIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Icon(
          Icons.shield_outlined,
          size: 152,
          color: Colors.blue.shade600,
        ),
        Icon(
          Icons.vpn_key_outlined,
          size: 56,
          color: Colors.green.shade400,
        ),
      ],
    );
  }
}
