import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/auto_fill/android_wear_manager.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/getit_config.dart';
import 'package:kee_pass_manager/init_page/presentation/manager/init_cubit.dart';
import 'package:kee_pass_manager/init_page/presentation/pages/init_page.dart';

void main() {
  GetItConfig.setup();

  WidgetsFlutterBinding.ensureInitialized();

  if (Platform.isAndroid) {
    AndroidAutofillManager.initialize();
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => DatabaseCubit(),
      child: MaterialApp(
        title: 'Password Manager',
        theme: ThemeData(
          useMaterial3: true,
          colorSchemeSeed: Colors.blue,
          brightness: Brightness.light,
        ),
        darkTheme: ThemeData(
          useMaterial3: true,
          colorSchemeSeed: Colors.blue,
          brightness: Brightness.dark,
        ),
        home: BlocProvider(
          create: (BuildContext context) => InitCubit(),
          child: InitPage(),
        ),
      ),
    );
  }
}
