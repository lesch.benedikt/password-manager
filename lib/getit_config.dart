import 'package:get_it/get_it.dart';
import 'package:kee_pass_manager/entry_list/data/data_sources/database_source.dart';
import 'package:kee_pass_manager/entry_list/data/repositories/database_repository.dart';
import 'package:kee_pass_manager/entry_list/domain/repositories/abstract_database_repository.dart';
import 'package:kee_pass_manager/import_csv/data/data_sources/csv_data_source.dart';
import 'package:kee_pass_manager/import_csv/data/repositories/csv_repository.dart';
import 'package:kee_pass_manager/import_csv/domain/repositories/abstract_csv_repository.dart';
import 'package:kee_pass_manager/import_kdbx/data/data_sources/kdbx_data_source.dart';
import 'package:kee_pass_manager/import_kdbx/data/repositories/kdbx_repository.dart';
import 'package:kee_pass_manager/import_kdbx/domain/repositories/abstract_kdbx_repository.dart';

class GetItConfig {
  static setup() {
    final getIt = GetIt.instance;

    getIt.registerSingleton<KdbxDataSource>(KdbxDataSource());
    getIt.registerSingleton<AbstractKdbxRepository>(KdbxRepository());

    getIt.registerSingleton<CsvDataSource>(CsvDataSource());
    getIt.registerSingleton<AbstractCsvRepository>(CsvRepository());

    getIt.registerSingleton<DatabaseSource>(DatabaseSource());
    getIt.registerSingleton<AbstractDatabaseRepository>(DatabaseRepository());
  }
}
