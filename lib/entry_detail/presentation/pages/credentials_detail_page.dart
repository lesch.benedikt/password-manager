import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/add_entry/presentation/pages/add_credentials_entry_page.dart';
import 'package:kee_pass_manager/entry_detail/domain/use_cases/entry_cubit_state.dart';
import 'package:kee_pass_manager/entry_detail/presentation/manager/entry_cubit.dart';
import 'package:kee_pass_manager/entry_detail/presentation/widgets/date_entry.dart';
import 'package:kee_pass_manager/entry_detail/presentation/widgets/delete_confirm_dialog.dart';
import 'package:kee_pass_manager/entry_detail/presentation/widgets/password_entry.dart';
import 'package:kee_pass_manager/entry_detail/presentation/widgets/text_entry.dart';
import 'package:kee_pass_manager/entry_detail/presentation/widgets/url_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credentials_entry.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';

class EntryDetailPage extends StatelessWidget {
  final String uuid;

  EntryDetailPage(this.uuid);

  @override
  Widget build(BuildContext context) {
    context.read<EntryCubit>().loadEntry(uuid);
    return BlocBuilder<EntryCubit, EntryCubitState>(
        builder: (context, snapshot) {
      if (snapshot is EntryCubitSuccessState) {
        return _buildSuccessBody(snapshot, context);
      } else {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
    });
  }

  Scaffold _buildSuccessBody(
      EntryCubitSuccessState snapshot, BuildContext context) {
    var entry = snapshot.entry as CredentialsEntry;
    return Scaffold(
      appBar: AppBar(
        title: Text(entry.title),
        actions: _buildActions(context, entry),
      ),
      body: Column(
        children: [
          TextEntry(entry.username, Icons.person_outline_rounded),
          PasswordEntry(entry.password, entry.salt, Icons.lock_outline_rounded),
          UrlEntry(entry.url, Icons.web_rounded),
          TextEntry(entry.notes, Icons.note_outlined),
          DateEntry(entry.createTime, Icons.timer_outlined),
          if (entry.expireTime != null)
            DateEntry(entry.expireTime!, Icons.timer_off_outlined),
        ],
      ),
    );
  }

  List<Widget> _buildActions(
      BuildContext context, CredentialsEntry credentialsEntry) {
    return [
      IconButton(
        icon: Icon(Icons.edit_outlined),
        onPressed: () => _onEditPressed(context, credentialsEntry),
      ),
      IconButton(
        icon: Icon(Icons.delete_outline_outlined),
        onPressed: () => _onDeletePressed(context, credentialsEntry),
      )
    ];
  }

  void _onEditPressed(BuildContext context, CredentialsEntry entry) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AddCredentialsEntryPage(entry)),
    );
  }

  void _onDeletePressed(BuildContext context, CredentialsEntry entry) async {
    var confirmed = await showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return DeleteConfirmDialog();
      },
    );

    if (confirmed == true) {
      context.read<DatabaseCubit>().deleteEntry(entry);
      Navigator.of(context).pop();
    }
  }
}
