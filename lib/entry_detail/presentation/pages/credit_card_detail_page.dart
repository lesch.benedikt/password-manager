import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/add_entry/presentation/pages/add_credit_card_page.dart';
import 'package:kee_pass_manager/entry_detail/domain/use_cases/entry_cubit_state.dart';
import 'package:kee_pass_manager/entry_detail/presentation/manager/entry_cubit.dart';
import 'package:kee_pass_manager/entry_detail/presentation/widgets/date_entry.dart';
import 'package:kee_pass_manager/entry_detail/presentation/widgets/delete_confirm_dialog.dart';
import 'package:kee_pass_manager/entry_detail/presentation/widgets/password_entry.dart';
import 'package:kee_pass_manager/entry_detail/presentation/widgets/text_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credit_card_entry.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/utils/string_utils.dart';

class CreditCardDetailPage extends StatelessWidget {
  final String uuid;

  CreditCardDetailPage(this.uuid);

  @override
  Widget build(BuildContext context) {
    context.read<EntryCubit>().loadEntry(uuid);
    return BlocBuilder<EntryCubit, EntryCubitState>(
        builder: (context, snapshot) {
      if (snapshot is EntryCubitSuccessState) {
        return _buildSuccessBody(context, snapshot.entry as CreditCardEntry);
      } else {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
    });
  }

  Scaffold _buildSuccessBody(BuildContext context, CreditCardEntry entry) {
    return Scaffold(
      appBar: AppBar(
        title: Text(entry.title),
        actions: [
          IconButton(
            icon: Icon(Icons.edit_outlined),
            onPressed: () => _onEditPressed(context, entry),
          ),
          IconButton(
            icon: Icon(Icons.delete_outline_outlined),
            onPressed: () => _onDeletePressed(context, entry),
          )
        ],
      ),
      body: Column(
        children: [
          TextEntry(StringUtils.formatCreditCardNo(entry.cardNo.toString()),
              Icons.credit_card),
          PasswordEntry(entry.pin, entry.salt, Icons.lock_outline_rounded),
          TextEntry(entry.notes, Icons.note_outlined),
          DateEntry(entry.createTime, Icons.timer_outlined),
          if (entry.expireTime != null)
            DateEntry(entry.expireTime!, Icons.timer_off_outlined),
        ],
      ),
    );
  }

  void _onEditPressed(BuildContext context, CreditCardEntry entry) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AddCreditCardPage(entry)),
    );
  }

  void _onDeletePressed(BuildContext context, CreditCardEntry entry) async {
    var confirmed = await showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return DeleteConfirmDialog();
      },
    );

    if (confirmed == true) {
      context.read<DatabaseCubit>().deleteEntry(entry);
      Navigator.of(context).pop();
    }
  }
}
