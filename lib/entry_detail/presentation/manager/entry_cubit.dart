import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:kee_pass_manager/entry_detail/domain/use_cases/entry_cubit_state.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/entry_list/domain/repositories/abstract_database_repository.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class EntryCubit extends Cubit<EntryCubitState> {
  EntryCubit() : super(EntryCubitLoadingState());

  void loadEntry(String uuid) async {
    emit(EntryCubitLoadingState());

    String password = await SecureStorage.readPassword();
    Database database =
        await GetIt.I<AbstractDatabaseRepository>().loadDatabase(password);

    var entry = database.findEntry(uuid);

    if (entry != null) {
      emit(EntryCubitSuccessState(entry));
    }
  }
}
