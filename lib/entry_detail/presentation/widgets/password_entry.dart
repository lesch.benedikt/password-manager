import 'dart:convert';
import 'dart:typed_data';

import 'package:encrypt/encrypt.dart';
import 'package:flutter/material.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class PasswordEntry extends StatefulWidget {
  final Uint8List passwordEncrypted;
  final String salt;
  final IconData icon;

  PasswordEntry(this.passwordEncrypted, this.salt, this.icon);

  @override
  _PasswordEntryState createState() => _PasswordEntryState();
}

class _PasswordEntryState extends State<PasswordEntry> {
  bool isHidden = true;
  String? passwordDecrypted;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Icon(widget.icon),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Text(
                isHidden ? "*****" : passwordDecrypted ?? "",
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
          ),
          IconButton(
            icon: Icon(isHidden
                ? Icons.visibility_off_outlined
                : Icons.visibility_outlined),
            onPressed: () {
              changeVisibility();
            },
          ),
        ],
      ),
    );
  }

  void changeVisibility() async {
    if (isHidden) {
      String password = await SecureStorage.readPassword();

      var codeUnits = await EncryptionUtil.decrypt(
          widget.passwordEncrypted,
          password,
          widget.salt,
          EncryptionUtil.PW_ITERATIONS,
          IV.fromLength(16));
      passwordDecrypted = utf8.decode(codeUnits);
    } else {
      passwordDecrypted = null;
    }
    setState(() {
      isHidden = !isHidden;
    });
  }
}
