import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlEntry extends StatelessWidget {
  final Uri? uri;
  final IconData icon;

  UrlEntry(this.uri, this.icon);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: [
          Icon(icon),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: new RichText(
                text: new TextSpan(
                  children: [
                    new TextSpan(
                      text: uri?.toString(),
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium
                          ?.copyWith(color: Colors.blue),
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () {
                          var localUri = uri;
                          if (localUri != null) {
                            launchUrl(localUri);
                          }
                        },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
