import 'package:flutter/material.dart';

class TextEntry extends StatelessWidget {
  final String string;
  final IconData icon;

  TextEntry(this.string, this.icon);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: [
          Icon(icon),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Text(
                string,
                style: Theme.of(context).textTheme.titleMedium,
                softWrap: true,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
