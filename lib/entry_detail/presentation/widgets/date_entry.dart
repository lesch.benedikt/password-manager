import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateEntry extends StatelessWidget {
  final DateTime dateTime;
  final IconData icon;

  DateEntry(this.dateTime, this.icon);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: [
          Icon(icon),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Text(
                _formateDate(),
                style: Theme.of(context).textTheme.titleMedium,
                softWrap: true,
              ),
            ),
          ),
        ],
      ),
    );
  }

  String _formateDate() {
    return "${DateFormat.yMd().format(dateTime)}-${DateFormat.Hms().format(dateTime)}";
  }
}
