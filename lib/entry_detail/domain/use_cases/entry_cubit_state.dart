import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';

abstract class EntryCubitState {}

class EntryCubitLoadingState extends EntryCubitState {}

class EntryCubitSuccessState extends EntryCubitState {
  final Entry entry;

  EntryCubitSuccessState(this.entry);
}
