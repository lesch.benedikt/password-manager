import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';

abstract class AbstractDatabaseRepository {
  saveDatabase(Database database, String password);

  Future<Database> loadDatabase(String password);
}
