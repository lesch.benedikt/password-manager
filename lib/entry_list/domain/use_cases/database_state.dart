import 'package:kee_pass_manager/common/domain/use_cases/error_state.dart';
import 'package:kee_pass_manager/common/domain/use_cases/loading_state.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';

abstract class DatabaseState {}

class DatabaseInitialState extends DatabaseState {}

class DatabaseLoadingState extends DatabaseState implements LoadingState {}

class DatabaseErrorState extends DatabaseState implements ErrorState {}

class DatabaseSuccessState extends DatabaseState {
  Database db;
  GroupEntry displayGroup;
  List<Entry> selectedItems;

  DatabaseSuccessState({
    required this.db,
    GroupEntry? displayGroup,
    List<Entry>? selectedItems,
  })  : this.displayGroup = displayGroup != null ? displayGroup : db.root,
        this.selectedItems = selectedItems != null ? selectedItems : [];

  DatabaseSuccessState copyWith({
    Database? db,
    GroupEntry? displayGroup,
    List<Entry>? selectedItems,
  }) {
    return DatabaseSuccessState(
      db: db ?? this.db,
      displayGroup: displayGroup ?? this.displayGroup,
      selectedItems: selectedItems ?? this.selectedItems,
    );
  }
}

class InvalidPasswordState extends DatabaseState {
  InvalidPasswordState();
}
