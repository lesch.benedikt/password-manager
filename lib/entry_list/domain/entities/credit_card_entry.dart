import 'dart:convert';
import 'dart:typed_data';

import 'package:encrypt/encrypt.dart';
import 'package:kee_pass_manager/auto_fill/account.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class CreditCardEntry extends Entry {
  int cardNo;
  Uint8List pin;
  String salt;
  String notes;
  DateTime createTime;
  DateTime? expireTime;

  CreditCardEntry(String uuid, String title, this.cardNo, this.pin, this.salt,
      this.notes, this.createTime,
      {String? parent, this.expireTime})
      : super(uuid, title, parent, false);


  @override
  Future<Account> getAutofillPair() async {
    String dbPassword = await SecureStorage.readPassword();
    String decyptedPin = utf8.decode(await EncryptionUtil.decrypt(pin,
        dbPassword, salt, EncryptionUtil.PW_ITERATIONS, IV.fromLength(16)));
    return Account(title, cardNo.toString(), decyptedPin);
  }

  Map<String, dynamic> toJson() => {
        'uuid': uuid,
        'title': title,
        'cardNo': cardNo,
        'pin': pin,
        'salt': salt,
        'notes': notes,
        'createTime': createTime.millisecondsSinceEpoch,
        'expireTime': expireTime?.millisecondsSinceEpoch,
        'parent': parent,
        'isGroup': isGroup,
      };

  CreditCardEntry.fromJson(Map<String, dynamic> json)
      : cardNo = json['cardNo'],
        pin = Uint8List.fromList(
            json['pin'].map<int>((entry) => entry as int).toList()),
        salt = json['salt'],
        notes = json['notes'],
        createTime = DateTime.fromMillisecondsSinceEpoch(json['createTime']),
        expireTime = json['expireTime'] != null
            ? DateTime.fromMillisecondsSinceEpoch(json['expireTime'])
            : null,
        super.fromJson(json);

  @override
  String toString() {
    return 'CreditCardEntry{id: $uuid, title: $title, parent: $parent}';
  }
}
