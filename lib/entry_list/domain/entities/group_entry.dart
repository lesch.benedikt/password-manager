import 'package:kee_pass_manager/entry_list/domain/entities/credentials_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credit_card_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';

class GroupEntry extends Entry {
  List<Entry> entries;

  GroupEntry(String uuid, String title, String? parent, this.entries)
      : super(uuid, title, parent, true);

  Map<String, dynamic> toJson() => {
        'uuid': uuid,
        'title': title,
        'parent': parent,
        'entries': entries.map((i) => i.toJson()).toList(),
        'isGroup': isGroup
      };

  GroupEntry.fromJson(Map<String, dynamic> json)
      : entries = json['entries'].map<Entry>((entry) {
    if (entry['isGroup']) {
            return GroupEntry.fromJson(entry);
          } else if (entry['cardNo'] != null) {
            //TODO introduce entry type
            return CreditCardEntry.fromJson(entry);
          } else {
            return CredentialsEntry.fromJson(entry);
          }
        }).toList(),
        super.fromJson(json);

  @override
  String toString() {
    return 'GroupEntry{id: $uuid, title: $title, parent: $parent, entries: $entries}';
  }
}
