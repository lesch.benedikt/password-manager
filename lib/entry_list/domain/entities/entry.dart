import 'package:kee_pass_manager/auto_fill/account.dart';

abstract class Entry implements Comparable<Entry> {
  String uuid;
  String title;
  bool isGroup;

  String? parent;

  Entry(this.uuid, this.title, this.parent, this.isGroup);

  Map<String, dynamic> toJson();

  Entry.fromJson(Map<String, dynamic> json)
      : uuid = json['uuid'],
        title = json['title'],
        parent = json['parent'],
        isGroup = json['isGroup'];

  @override
  int compareTo(Entry other) {
    if (isGroup == other.isGroup) {
      return title.toLowerCase().compareTo(other.title.toLowerCase());
    } else if (isGroup) {
      return -1;
    } else {
      return 1;
    }
  }

  bool matches(String needle){
    return title.contains(needle);
  }

  Future<Account>? getAutofillPair(){
    return null;
  }
}
