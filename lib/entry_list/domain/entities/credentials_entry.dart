import 'dart:convert';
import 'dart:typed_data';

import 'package:encrypt/encrypt.dart';
import 'package:kee_pass_manager/auto_fill/account.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class CredentialsEntry extends Entry {
  String username;
  Uint8List password;
  String salt;
  Uri? url;
  String notes;
  DateTime createTime;
  DateTime? expireTime;

  CredentialsEntry(String uuid, String title, this.username, this.password,
      this.salt, this.url, this.notes, this.createTime,
      {String? parent, this.expireTime})
      : super(uuid, title, parent, false);

  //TODO parent not null, root default

  bool matches(String needle){
    return title.contains(needle) ||
        url.toString().contains(needle) ||
        notes.contains(needle);
  }

  @override
  Future<Account> getAutofillPair() async {
    String dbPassword = await SecureStorage.readPassword();
    String decryptedPassword = utf8.decode(await EncryptionUtil.decrypt(
        password,
        dbPassword,
        salt,
        EncryptionUtil.PW_ITERATIONS,
        IV.fromLength(16)));
    return Account(title, username, decryptedPassword);
  }

  Map<String, dynamic> toJson() => {
        'uuid': uuid,
        'title': title,
        'username': username,
        'password': password,
        'salt': salt,
        'url': url,
        'notes': notes,
        'createTime': createTime.millisecondsSinceEpoch,
        'expireTime': expireTime?.millisecondsSinceEpoch,
        'parent': parent,
        'isGroup': isGroup,
      };

  CredentialsEntry.fromJson(Map<String, dynamic> json)
      : username = json['username'],
        password = Uint8List.fromList(
            json['password'].map<int>((entry) => entry as int).toList()),
        salt = json['salt'],
        url = json['url'],
        notes = json['notes'],
        createTime = DateTime.fromMillisecondsSinceEpoch(json['createTime']),
        expireTime = json['expireTime'] != null
            ? DateTime.fromMillisecondsSinceEpoch(json['expireTime'])
            : null,
        super.fromJson(json);

  @override
  String toString() {
    return 'CredentialsEntry{id: $uuid, title: $title, parent: $parent}';
  }
}
