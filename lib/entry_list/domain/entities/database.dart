import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';

class Database {
  String path;
  GroupEntry root;

  Database({required this.path, required this.root});

  Map<String, dynamic> toJson() => {
        'path': path,
        'root': root.toJson(),
      };

  Database.fromJson(Map<String, dynamic> json)
      : path = json['path'],
        root = GroupEntry.fromJson(json['root']);

  @override
  String toString() {
    return 'Database{path: $path, root: $root}';
  }

  GroupEntry? findGroup(String uuid) {
    if (root.uuid == uuid) {
      return root;
    }
    return _findGroup(uuid, root);
  }

  GroupEntry? _findGroup(String uuid, GroupEntry group) {
    for (var element in group.entries) {
      if (element.isGroup) {
        var group = element as GroupEntry;
        if (group.uuid == uuid) {
          return group;
        } else {
          var childSearch = _findGroup(uuid, element);
          if (childSearch != null) {
            return childSearch;
          }
        }
      }
    }
    return null;
  }

  Entry? findEntry(String uuid) {
    return _findEntry(uuid, root);
  }

  Entry? _findEntry(String uuid, GroupEntry group) {
    for (var element in group.entries) {
      if (element.uuid == uuid) {
        return element;
      } else if (element is GroupEntry) {
        var childSearch = _findEntry(uuid, element);
        if (childSearch != null) {
          return childSearch;
        }
      }
    }
    return null;
  }

  void addEntries(List<Entry> entries) {
    for (var newEntry in entries) {
      newEntry.parent = root.uuid;
      root.entries.add(newEntry);
    }
  }

  void addEntry(Entry entry) {
    var parent = findGroup(entry.parent!);
    parent?.entries.add(entry);
  }

  Database copyWith({
    String? path,
    GroupEntry? root,
  }) {
    return Database(
      path: path ?? this.path,
      root: root ?? this.root,
    );
  }

  void removeEntry(Entry entry) {
    var parentUuid = entry.parent;
    if (parentUuid != null) {
      GroupEntry? parent = findGroup(parentUuid);
      if (parent != null) {
        parent.entries.removeWhere((element) => element.uuid == entry.uuid);
      }
    }
  }
}
