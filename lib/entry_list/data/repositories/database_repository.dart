import 'package:get_it/get_it.dart';
import 'package:kee_pass_manager/entry_list/data/data_sources/database_source.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/entry_list/domain/repositories/abstract_database_repository.dart';

class DatabaseRepository extends AbstractDatabaseRepository {
  var _dataSource = GetIt.I<DatabaseSource>();

  Future<void> saveDatabase(Database database, String password) async {
    await _dataSource.saveDatabase(database, password);
  }

  Future<Database> loadDatabase(String password) async {
    return _dataSource.loadDatabase(password);
  }
}
