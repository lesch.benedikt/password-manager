import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';

class DatabaseSource {
  Future<void> saveDatabase(Database database, String password) async {
    String path = await SecureStorage.readPath() ?? "";

    Uint8List encryptedDB = await EncryptionUtil.encryptDb(database, password);

    var writeFile = File(path);
    writeFile.writeAsBytesSync(encryptedDB);
  }

  Future<Database> loadDatabase(String password) async {
    String path = await SecureStorage.readPath() ?? "";

    var readFile = File(path);
    Uint8List readBytes = readFile.readAsBytesSync();

    String string = await EncryptionUtil.decryptDb(readBytes, password);

    var database = Database.fromJson(jsonDecode(string));

    return database;
  }
}
