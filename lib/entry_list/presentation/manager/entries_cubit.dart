import 'package:bloc/bloc.dart';
import 'package:encrypt/encrypt.dart';
import 'package:get_it/get_it.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credentials_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credit_card_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/repositories/abstract_database_repository.dart';
import 'package:kee_pass_manager/entry_list/domain/use_cases/database_state.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:kee_pass_manager/utils/salt.dart';
import 'package:kee_pass_manager/utils/secure_storage.dart';
import 'package:uuid/uuid.dart';

class DatabaseCubit extends Cubit<DatabaseState> {
  DatabaseCubit() : super(DatabaseInitialState());

  loadEntries(String password) async {
    emit(DatabaseLoadingState());

    try {
      var db =
          await GetIt.I<AbstractDatabaseRepository>().loadDatabase(password);
      emit(DatabaseSuccessState(db: db));
    } catch (e) {
      emit(DatabaseErrorState());
    }
  }

  selectedGroup(GroupEntry entry) {
    var successState = (state as DatabaseSuccessState);
    emit(DatabaseLoadingState());

    var newState = successState.copyWith(displayGroup: entry);

    emit(newState);
  }

  bool selectParentGroup() {
    var successState = (state as DatabaseSuccessState);
    emit(DatabaseLoadingState());

    var groupEntry = successState.displayGroup;
    var parent = groupEntry.parent;
    if (parent != null) {
      var group = successState.db.findGroup(parent);
      emit(successState.copyWith(displayGroup: group));
      return false;
    } else {
      return true;
    }
  }

  addEntry(Entry entry) async {
    var successState = (state as DatabaseSuccessState);
    emit(DatabaseLoadingState());

    var selectedGroup = successState.db.findGroup(entry.parent!);
    if (selectedGroup != null) {
      List<Entry> entries = selectedGroup.entries;
      entry.parent = selectedGroup.uuid;
      entries.add(entry);
    }

    await _save(successState.db);

    emit(successState.copyWith());
  }

  addCredentials(
      String uuid, String title, String userName, String password) async {
    var successState = (state as DatabaseSuccessState);
    emit(DatabaseLoadingState());

    String dbPassword = await SecureStorage.readPassword();
    String salt = Salt.generateAsBase64String(64);
    var passwortEncypted = await EncryptionUtil.encrypt(password, dbPassword,
        salt, EncryptionUtil.PW_ITERATIONS, IV.fromLength(16));

    var entry = CredentialsEntry(
        uuid, title, userName, passwortEncypted, salt, null, "", DateTime.now(),
        parent: successState.displayGroup.uuid);

    successState.db.addEntry(entry);

    await _save(successState.db);

    emit(successState.copyWith());
  }

  addCreditCard(String uuid, String title, String cardNo, String pin) async {
    var successState = (state as DatabaseSuccessState);
    emit(DatabaseLoadingState());

    String dbPassword = await SecureStorage.readPassword();
    String salt = Salt.generateAsBase64String(64);
    var pinEncrypted = await EncryptionUtil.encrypt(
        pin, dbPassword, salt, EncryptionUtil.PW_ITERATIONS, IV.fromLength(16));

    var entry = CreditCardEntry(
        uuid, title, int.parse(cardNo), pinEncrypted, salt, "", DateTime.now(),
        parent: successState.displayGroup.uuid);

    successState.db.addEntry(entry);

    await _save(successState.db);

    emit(successState.copyWith());
  }

  addGroup(String name) async {
    var successState = (state as DatabaseSuccessState);
    emit(DatabaseLoadingState());

    var entries = successState.displayGroup.entries;

    name = name.trim();
    var newGroup =
        GroupEntry(Uuid().v4(), name, successState.displayGroup.uuid, []);

    entries.add(newGroup);

    await _save(successState.db);

    emit(successState.copyWith());
  }

  deleteEntry(Entry entry, [bool loading = true]) async {
    var successState = (state as DatabaseSuccessState);
    if (loading) {
      emit(DatabaseLoadingState());
    }

    var parent = successState.db.findGroup(entry.parent!);
    parent?.entries.removeWhere((element) => element.uuid == entry.uuid);

    await _save(successState.db);

    if (loading) {
      emit(successState.copyWith());
    }
  }

  _save(Database db) async {
    String password = await SecureStorage.readPassword();
    await GetIt.I<AbstractDatabaseRepository>().saveDatabase(db, password);
  }

  selectItem(Entry entry) async {
    var successState = (state as DatabaseSuccessState);
    emit(DatabaseLoadingState());

    if (successState.selectedItems.contains(entry))
      successState.selectedItems.remove(entry);
    else
      successState.selectedItems.add(entry);

    emit(successState.copyWith());
  }

  void clearSelection() {
    var successState = (state as DatabaseSuccessState);
    emit(DatabaseLoadingState());

    successState.selectedItems.clear();

    emit(successState.copyWith());
  }
}
