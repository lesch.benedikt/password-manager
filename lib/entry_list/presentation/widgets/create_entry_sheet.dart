import 'package:flutter/material.dart';
import 'package:kee_pass_manager/add_entry/presentation/pages/add_credentials_entry_page.dart';
import 'package:kee_pass_manager/add_entry/presentation/pages/add_credit_card_page.dart';

class CreateEntrySheet extends StatelessWidget {
  final Function onItemClicked;

  CreateEntrySheet(this.onItemClicked);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          leading: new Icon(Icons.folder_open_outlined),
          title: new Text('Group'),
          onTap: () {
            Navigator.pop(context);
            onItemClicked();
          },
        ),
        Divider(),
        ListTile(
          leading: new Icon(Icons.vpn_key_outlined),
          title: new Text('Credentials'),
          onTap: () {
            Navigator.pop(context);
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AddCredentialsEntryPage()),
            );
          },
        ),
        ListTile(
          leading: new Icon(Icons.credit_card),
          title: new Text('Credit card'),
          onTap: () {
            Navigator.pop(context);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddCreditCardPage()),
            );
          },
        ),
      ],
    );
  }
}
