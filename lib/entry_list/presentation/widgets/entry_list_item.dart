import 'package:flutter/material.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credentials_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credit_card_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';

class EntryItem extends StatefulWidget {
  final Entry object;
  final bool isSelected;
  final Function(Entry) onItemClicked;
  final Function(Entry) onLongClick;

  EntryItem(this.object, this.isSelected, this.onItemClicked, this.onLongClick);

  @override
  State<EntryItem> createState() => _EntryItemState();
}

class _EntryItemState extends State<EntryItem> {
  @override
  Widget build(BuildContext context) {
    return _buildItem(context, widget.object);
  }

  Widget _buildItem(BuildContext context, Entry kdbxObject) {
    if (kdbxObject is GroupEntry) {
      return _buildGroupItem(context, kdbxObject);
    } else if (kdbxObject is CredentialsEntry) {
      return _buildEntryItem(context, kdbxObject);
    } else if (kdbxObject is CreditCardEntry) {
      return _buildCreditCardItem(context, kdbxObject);
    }

    return Container();
  }

  ListTile _buildEntryItem(BuildContext context, CredentialsEntry entry) {
    return _buildTile(context, entry, Icons.vpn_key_outlined);
  }

  ListTile _buildCreditCardItem(BuildContext context, CreditCardEntry entry) {
    return _buildTile(context, entry, Icons.credit_card);
  }

  ListTile _buildGroupItem(BuildContext context, GroupEntry entry) {
    return _buildTile(context, entry, Icons.folder_open_rounded);
  }

  ListTile _buildTile(BuildContext context, Entry entry, IconData icon) {
    return ListTile(
      leading: widget.isSelected
          ? _buildSelectedIcon(entry)
          : _buildIcon(icon, entry),
      title: Text(entry.title),
      onTap: () => widget.onItemClicked(entry),
      onLongPress: () => widget.onLongClick(entry),
    );
  }

  Widget _buildIcon(IconData icon, Entry entry) {
    return InkWell(
      child: IconButton(
        icon: Icon(icon),
        onPressed: () => widget.onLongClick(entry),
      ),
      onTap: () => widget.onLongClick(entry),
    );
  }

  Widget _buildSelectedIcon(Entry entry) {
    return Padding(
      padding: const EdgeInsets.only(left: 4, right: 4),
      child: CircleAvatar(
        backgroundColor: Theme.of(context).colorScheme.secondary,
        child: InkWell(
          child: Icon(
            Icons.check,
            color: Theme.of(context).colorScheme.onSecondary,
            size: 16,
          ),
          onTap: () => widget.onLongClick(entry),
        ),
      ),
    );
  }
}
