import 'package:flutter/material.dart';

class CreateGroupDialog extends StatefulWidget {
  @override
  State<CreateGroupDialog> createState() => _CreateGroupDialogState();
}

class _CreateGroupDialogState extends State<CreateGroupDialog> {
  final textController = TextEditingController();

  final key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Add group'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Form(
            key: key,
            child: TextFormField(
              controller: textController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Group name',
                isDense: true,
              ),
              validator: (value) {
                if (value == null || value.trim().isEmpty) {
                  return 'Group name cannot be empty';
                }
                return null;
              },
            ),
          )
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Cancel'),
        ),
        TextButton(
          onPressed: () {
            if (key.currentState!.validate()) {
              Navigator.pop(
                context,
                textController.text,
              );
            }
          },
          child: Text('Ok'),
        )
      ],
    );
  }
}
