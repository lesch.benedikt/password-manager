import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/use_cases/database_state.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';

class Breadcrumbs extends StatelessWidget {
  final Database db;
  final GroupEntry selectedItem;

  Breadcrumbs(this.db, this.selectedItem);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DatabaseCubit, DatabaseState>(builder: (context, state) {
      if (state is DatabaseSuccessState) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            _buildBreadcrumbs(),
            style: Theme.of(context)
                .textTheme
                .bodyLarge
                ?.copyWith(color: Theme.of(context).disabledColor),
          ),
        );
      } else {
        return Container();
      }
    });
  }

  String _buildBreadcrumbs() {
    String asd = " \u{2022} ${selectedItem.title}";
    Entry currentItem = selectedItem;
    while (currentItem.parent != null) {
      currentItem = db.findGroup(currentItem.parent!)!;
      asd = " \u{2022} ${currentItem.title}" + asd;
    }
    return asd;
  }
}
