import 'package:flutter/material.dart';
import 'package:kee_pass_manager/common/domain/entities/list_item.dart';

class EntryList<T> extends StatefulWidget {
  final List<ListItem<T>> listItems;
  final Function(T entry)? onItemClicked;
  final Function(T entry)? onItemDeleted;
  final Widget Function(ListItem<T> entry) itemBuilder;

  EntryList(
      {required this.listItems,
      required this.itemBuilder,
      this.onItemClicked,
      this.onItemDeleted});

  @override
  State<EntryList> createState() => _EntryListState<T>();
}

class _EntryListState<T> extends State<EntryList<T>> {
  ListItem<T>? markedForDeletion;

  @override
  Widget build(BuildContext context) {
    widget.listItems.sort();
    return ListView.builder(
      shrinkWrap: true,
      itemCount: widget.listItems.length,
      itemBuilder: (context, pos) {
        var item = widget.listItems.elementAt(pos);

        return _buildDismissible(context, item);
      },
    );
  }

  Dismissible _buildDismissible(BuildContext context, ListItem<T> item) {
    return Dismissible(
      onDismissed: (dir) => _onDismissed(context, item),
      key: item.key,
      child: widget.itemBuilder(item),
    );
  }

  _onDismissed(BuildContext context, ListItem<T> item) {
    if (markedForDeletion != null) {
      widget.onItemDeleted?.call(item.item);
    }

    markedForDeletion = item;
    ScaffoldMessenger.of(context)
        .showSnackBar(_buildSnackbar(context))
        .closed
        .then(
      (value) {
        markedForDeletion = null;
        widget.onItemDeleted?.call(item.item);
      },
    );
  }

  SnackBar _buildSnackbar(BuildContext context) {
    return SnackBar(
      content: Text("Item deleted"),
      action: SnackBarAction(
        label: "Undo",
        onPressed: () {
          if (markedForDeletion != null) {
            setState(() {
              widget.listItems.remove(markedForDeletion!);
              widget.listItems.add(ListItem(
                  markedForDeletion!.item, markedForDeletion!.isSelected));
              markedForDeletion = null;
            });
          }
        },
      ),
    );
  }
}
