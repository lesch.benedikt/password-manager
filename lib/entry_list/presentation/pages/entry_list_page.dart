import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kee_pass_manager/common/domain/entities/list_item.dart';
import 'package:kee_pass_manager/common/presentation/widgets/default_bloc_builder.dart';
import 'package:kee_pass_manager/entry_detail/presentation/manager/entry_cubit.dart';
import 'package:kee_pass_manager/entry_detail/presentation/pages/credentials_detail_page.dart';
import 'package:kee_pass_manager/entry_detail/presentation/pages/credit_card_detail_page.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credentials_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/credit_card_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/entry.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';
import 'package:kee_pass_manager/entry_list/domain/use_cases/database_state.dart';
import 'package:kee_pass_manager/entry_list/presentation/manager/entries_cubit.dart';
import 'package:kee_pass_manager/entry_list/presentation/widgets/breadcrumbs.dart';
import 'package:kee_pass_manager/entry_list/presentation/widgets/create_entry_sheet.dart';
import 'package:kee_pass_manager/entry_list/presentation/widgets/create_group_dialog.dart';
import 'package:kee_pass_manager/entry_list/presentation/widgets/entry_list.dart';
import 'package:kee_pass_manager/entry_list/presentation/widgets/entry_list_item.dart';
import 'package:kee_pass_manager/import_csv/presentation/manager/import_csv_cubit.dart';
import 'package:kee_pass_manager/import_csv/presentation/pages/import_csv_page.dart';
import 'package:kee_pass_manager/import_kdbx/presentation/manager/import_kdbx_cubit.dart';
import 'package:kee_pass_manager/import_kdbx/presentation/pages/import_kdbx_page.dart';
import 'package:kee_pass_manager/move_items/presentation/manager/move_items_cubit.dart';
import 'package:kee_pass_manager/move_items/presentation/pages/move_items_page.dart';
import 'package:kee_pass_manager/settings/presentation/manager/settings_cubit.dart';
import 'package:kee_pass_manager/settings/presentation/pages/settings_pages.dart';

class EntryListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultBlocBuilder<DatabaseCubit, DatabaseState>(
      builder: (context, state) {
        if (state is DatabaseSuccessState) {
          return _build(context, state);
        } else {
          return Container(
            child: Center(
              child: Text("ASD $state"),
            ),
          );
        }
      },
    );
  }

  WillPopScope _build(BuildContext context, DatabaseSuccessState state) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: Scaffold(
        appBar: _buildAppBar(context, state),
        body: _buildBody(context, state),
        floatingActionButton: _buildFab(context),
      ),
    );
  }

  AppBar _buildAppBar(BuildContext context, DatabaseSuccessState state) {
    return AppBar(
      title: state.selectedItems.isNotEmpty
          ? Text("${state.selectedItems.length} Selected")
          : Text("Password Manager"),
      leading: _buildLeading(state, context),
      actions: _buildActions(context, state),
    );
  }

  Widget _buildLeading(DatabaseSuccessState state, BuildContext context) {
    if (state.selectedItems.isNotEmpty || state.displayGroup.parent != null) {
      return IconButton(
        icon: Icon(Icons.arrow_back_rounded),
        onPressed: () {
          if (state.selectedItems.isNotEmpty) {
            context.read<DatabaseCubit>().clearSelection();
          } else {
            context.read<DatabaseCubit>().selectParentGroup();
          }
        },
      );
    } else {
      return Icon(Icons.home);
    }
  }

  List<Widget> _buildActions(BuildContext context, DatabaseSuccessState state) {
    if (state.selectedItems.isNotEmpty)
      return [
        IconButton(
            icon: Icon(Icons.drive_file_move),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return BlocProvider(
                    create: (BuildContext context) => MoveItemsCubit(),
                    child: MoveItemsPage(state.selectedItems),
                  );
                }),
              );
            })
      ];
    else {
      return [
        IconButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return BlocProvider(
                    create: (BuildContext context) => SettingsCubit(),
                    child: SettingsPage(),
                  );
                }),
              );
            },
            icon: Icon(Icons.settings)),
        PopupMenuButton<int>(
          onSelected: (index) {
            if (index == 0) {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return BlocProvider(
                    create: (BuildContext context) => ImportKdbxCubit(),
                    child: ImportKdbxPage(),
                  );
                }),
              );
            } else if (index == 1) {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return BlocProvider(
                    create: (BuildContext context) => ImportCsvCubit(),
                    child: ImportCsvPage(),
                  );
                }),
              );
            }
          },
          itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem<int>(
                value: 0,
                child: Text("Import from KeePass"),
              ),
              PopupMenuItem<int>(
                value: 1,
                child: Text("Import from CSV"),
              ),
            ];
          },
        ),
      ];
    }
  }

  Widget _buildBody(BuildContext context, DatabaseSuccessState state) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Breadcrumbs(state.db, state.displayGroup),
        Divider(height: 0),
        Expanded(
          child: _buildList(context, state),
        ),
      ],
    );
  }

  FloatingActionButton _buildFab(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        _showBottomSheet(context);
      },
    );
  }

  Widget _buildList(BuildContext context, DatabaseSuccessState state) {
    return BlocBuilder<DatabaseCubit, DatabaseState>(
      builder: (context, state) {
        if (state is DatabaseSuccessState) {
          return EntryList<Entry>(
            listItems: _createListItems(state),
            itemBuilder: (item) => _buildItem(context, item),
            onItemClicked: (item) {
              _onItemClicked(context, item);
            },
            onItemDeleted: (item) {
              context.read<DatabaseCubit>().deleteEntry(item, false);
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  List<ListItem<Entry>> _createListItems(DatabaseSuccessState state) {
    return state.displayGroup.entries
        .map((e) => ListItem(e, state.selectedItems.contains(e)))
        .toList();
  }

  EntryItem _buildItem(BuildContext context, ListItem<Entry> entry) {
    return EntryItem(
      entry.item,
      entry.isSelected,
      (item) => _onItemClicked(context, entry.item),
      (item) {
        context.read<DatabaseCubit>().selectItem(item);
      },
    );
  }

  _onItemClicked(BuildContext context, Entry item) {
    var databaseState = context.read<DatabaseCubit>().state;
    if (databaseState is DatabaseSuccessState &&
        databaseState.selectedItems.isNotEmpty) {
      context.read<DatabaseCubit>().selectItem(item);
    } else if (item is GroupEntry) {
      context.read<DatabaseCubit>().selectedGroup(item);
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) {
          return BlocProvider(
            create: (BuildContext context) => EntryCubit(),
            child: _buildNavigationChild(item, context),
          );
        }),
      );
    }
  }

  Widget _buildNavigationChild(Entry item, BuildContext context) {
    if (item is CredentialsEntry) {
      return EntryDetailPage(item.uuid);
    } else if (item is CreditCardEntry) {
      return CreditCardDetailPage(item.uuid);
    }

    throw Exception("Unknown item $item");
  }

  _showBottomSheet(BuildContext rootContext) {
    showModalBottomSheet(
      context: rootContext,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16.0),
          topRight: Radius.circular(16.0),
        ),
      ),
      builder: (context) {
        return CreateEntrySheet(() {
          _showAddGroupDialog(rootContext);
        });
      },
    );
  }

  _showAddGroupDialog(BuildContext context) async {
    String groupName = await showDialog(
      context: context,
      builder: (context) {
        return CreateGroupDialog();
      },
    );

    context.read<DatabaseCubit>().addGroup(groupName);
  }

  Future<bool> _onBackPressed(BuildContext context) async {
    return context.read<DatabaseCubit>().selectParentGroup();
  }
}
