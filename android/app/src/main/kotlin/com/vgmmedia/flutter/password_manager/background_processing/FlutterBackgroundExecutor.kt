package com.vgmmedia.flutter.password_manager.background_processing

import android.content.Context
import android.content.Intent
import android.os.Build
import android.service.autofill.FillResponse
import android.service.autofill.SaveInfo
import android.util.Log
import androidx.annotation.RequiresApi
import com.vgmmedia.flutter.password_manager.PluginRegistrantException
import com.vgmmedia.flutter.password_manager.autofill.AutofillPackage
import com.vgmmedia.flutter.password_manager.autofill.ResponseBuilder
import com.vgmmedia.flutter.password_manager.autofill.savedCallback
import com.vgmmedia.flutter.password_manager.autofill.savedRequest
import io.flutter.FlutterInjector
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.dart.DartExecutor.DartCallback
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.JSONMethodCodec
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.view.FlutterCallbackInformation
import org.json.JSONObject
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicBoolean

class FlutterBackgroundExecutor : MethodCallHandler {
    private var backgroundChannel: MethodChannel? = null
    private var backgroundFlutterEngine: FlutterEngine? = null
    private val isCallbackDispatcherReady = AtomicBoolean(false)
    val isRunning: Boolean
        get() = isCallbackDispatcherReady.get()

    private fun onInitialized() {
        isCallbackDispatcherReady.set(true)
        MessageService.onInitialized()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        val method = call.method
        try {
            when (method) {
                "initialized" -> {
                    Log.d("Autofill", "initialized")
                    onInitialized()
                    result.success(true)
                }
                "passAccount" -> {
                    val request = savedRequest
                    if (request != null && !request.alreadyCalled.getAndSet(true)) {
                        savedCallback?.let {
                            Log.d("Autofill", "Account received: ${call.arguments}")
                            Log.d("Autofill", "Saved request; $request")
                            val decodedAccount = call.arguments<JSONObject>()
                            Log.d("Autofill", "Decoded: $decodedAccount")

                            val password = request.password
                            val login = request.login
                            if (decodedAccount != null) {
                                Log.d("Autofill", "Show")
                                val value = AutofillPackage(
                                        decodedAccount.getString("title"),
                                        login,
                                        decodedAccount.getString("username"),
                                        password,
                                        decodedAccount.getString("password")
                                )
                                ResponseBuilder.buildResponse(
                                        it,
                                        listOf(value),
                                        request.ownPackageName!! //TODO
                                )
                            } else {
                                Log.d("Autofill", "Save")

                                val fillResponse: FillResponse = FillResponse.Builder()
                                        .setSaveInfo(
                                                SaveInfo.Builder(
                                                        SaveInfo.SAVE_DATA_TYPE_USERNAME or SaveInfo.SAVE_DATA_TYPE_PASSWORD,
                                                        arrayOf(login, password)
                                                ).build()
                                        )
                                        .build()

                                it.onSuccess(fillResponse)
                            }

                        }
                    }
                }
                else -> {
                    result.notImplemented()
                }
            }
        } catch (e: PluginRegistrantException) {
            result.error("error", "AlarmManager error: " + e.message, null)
        }
    }

    fun startBackgroundIsolate(context: Context) {
        if (!isRunning) {
            val p = context.getSharedPreferences(MessageService.SHARED_PREFERENCES_KEY, 0)
            val callbackHandle = p.getLong(CALLBACK_HANDLE_KEY, 0)
            startBackgroundIsolate(context, callbackHandle)
        }
    }

    fun startBackgroundIsolate(context: Context, callbackHandle: Long) {
        if (backgroundFlutterEngine != null) {
            Log.e(TAG, "Background isolate already started")
            return
        }
        Log.i(TAG, "Starting AlarmService...")
        val appBundlePath = FlutterInjector.instance().flutterLoader().findAppBundlePath()
        val assets = context.assets
        if (!isRunning) {
            backgroundFlutterEngine = FlutterEngine(context)
            val flutterCallback =
                    FlutterCallbackInformation.lookupCallbackInformation(callbackHandle)
            val executor = backgroundFlutterEngine!!.dartExecutor
            initializeMethodChannel(executor)
            val dartCallback = DartCallback(assets, appBundlePath, flutterCallback)
            executor.executeDartCallback(dartCallback)
        }
    }

    fun callFlutterMethod(latch: CountDownLatch?, intent: Intent) {
        Log.d("Autofill", "callFlutterMethod")
        var result: MethodChannel.Result? = null
        if (latch != null) {
            result = object : MethodChannel.Result {
                override fun success(result: Any?) {
                    latch.countDown()
                }

                override fun error(errorCode: String, errorMessage: String?, errorDetails: Any?) {
                    latch.countDown()
                }

                override fun notImplemented() {
                    latch.countDown()
                }
            }
        }
        when (intent.action) {
            "Search" -> {
                val packageNames = intent.getStringExtra("packageNames")
                val domains = intent.getStringExtra("domains")

                backgroundChannel?.invokeMethod(
                    "searchAccount",
                    arrayOf<Any?>(packageNames, domains),
                    result
                )
            }
            "Save" -> {
                val login = intent.getStringExtra("login")
                val password = intent.getStringExtra("password")
                val packageNames = intent.getStringExtra("packageNames")
                val domains = intent.getStringExtra("domains")

                backgroundChannel?.invokeMethod(
                    "saveAccount",
                    arrayOf<Any?>(login, password, packageNames, domains),
                    result
                )
            }
        }
    }

    private fun initializeMethodChannel(isolate: BinaryMessenger) {
        backgroundChannel = MethodChannel(
                isolate,
                "com.vgmmedia.flutter.expenses/background",
                JSONMethodCodec.INSTANCE
        )
        backgroundChannel?.setMethodCallHandler(this)
    }

    companion object {
        private const val TAG = "BackgroundExecutor"
        const val CALLBACK_HANDLE_KEY = "callback_handle"
    }
}