package com.vgmmedia.flutter.password_manager

import io.flutter.app.FlutterApplication

/**
 * Needed for working background handling
 */
class Application : FlutterApplication() {
    override fun onCreate() {
        super.onCreate()
    }
}