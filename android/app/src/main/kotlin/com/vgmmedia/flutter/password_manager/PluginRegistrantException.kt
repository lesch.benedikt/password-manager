package com.vgmmedia.flutter.password_manager

import java.lang.RuntimeException

internal class PluginRegistrantException : RuntimeException(
        "PluginRegistrantCallback is not set. Did you forget to call "
                + "AlarmService.setPluginRegistrant? See the README for instructions."
)