package com.vgmmedia.flutter.password_manager.autofill

import android.annotation.TargetApi
import android.app.assist.AssistStructure
import android.os.Build
import java.util.*

data class AutofillHeuristic(
    val weight: Int,
    val predicate: (node: AssistStructure.ViewNode) -> Boolean
)

fun MutableList<AutofillHeuristic>.heuristic(
    weight: Int,
    predicate: (node: AssistStructure.ViewNode) -> Boolean
): Boolean {
    return add(AutofillHeuristic(weight, predicate))
}


@TargetApi(Build.VERSION_CODES.O)
fun MutableList<AutofillHeuristic>.autofillHint(weight: Int, hint: String): Boolean {
    return heuristic(weight) {
        it.autofillHints?.contains(hint) == true
    }
}

@TargetApi(Build.VERSION_CODES.O)
fun MutableList<AutofillHeuristic>.idEntry(weight: Int, match: String): Boolean {
    return heuristic(weight) {
        it.idEntry == match
    }
}

@TargetApi(Build.VERSION_CODES.O)
fun MutableList<AutofillHeuristic>.htmlAttribute(weight: Int, attr: String, value: String): Boolean {
    return heuristic(weight) {
        it.htmlInfo?.attributes?.firstOrNull {
                attribute -> attribute.first == attr && attribute.second == value
        } != null
    }
}

@TargetApi(Build.VERSION_CODES.O)
fun MutableList<AutofillHeuristic>.defaults(hint: String, match: String) {
    autofillHint(900, hint)
    idEntry(800, match)
    heuristic(700) { it.idEntry?.lowercase(Locale.ROOT)?.contains(match) == true }
}

fun Int?.hasFlag(flag: Int) = this != null && flag and this == flag