package com.vgmmedia.flutter.password_manager.autofill

import android.os.Build
import android.service.autofill.Dataset
import android.service.autofill.FillCallback
import android.service.autofill.FillResponse
import android.view.autofill.AutofillId
import android.view.autofill.AutofillValue
import android.widget.RemoteViews
import androidx.annotation.RequiresApi
import com.vgmmdia.flutter.password_manager.R

object ResponseBuilder {

    @RequiresApi(Build.VERSION_CODES.O)
    fun buildResponse(callback: FillCallback, list: List<AutofillPackage>, packageName: String) {
        val fillResponse: FillResponse.Builder = FillResponse.Builder()
        list.forEach {

            val userNamePresentation = RemoteViews(
                packageName,
                R.layout.multidataset_service_list_item
            )

            val passwordPresentation = RemoteViews(
                packageName,
                R.layout.multidataset_service_list_item
            )

            userNamePresentation.setTextViewText(R.id.text, it.username)
            userNamePresentation.setTextViewText(R.id.app, it.accountName)
            passwordPresentation.setTextViewText(R.id.text, it.username)
            passwordPresentation.setTextViewText(R.id.app, it.accountName)

            fillResponse.addDataset(
                Dataset.Builder()
                    .setValue(
                        it.usernameId,
                        AutofillValue.forText(it.username),
                        userNamePresentation
                    )
                    .setValue(
                        it.passwordId,
                        AutofillValue.forText(it.password),
                        passwordPresentation
                    )
                    .build()
            )
        }

        callback.onSuccess(fillResponse.build())
    }
}

data class AutofillPackage(
    val accountName: String,
    val usernameId: AutofillId,
    val username: String,
    val passwordId: AutofillId,
    val password: String
)