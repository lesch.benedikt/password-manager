package com.vgmmedia.flutter.password_manager.background_processing

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.util.Log
import androidx.core.app.JobIntentService
import androidx.core.content.edit
import com.vgmmedia.flutter.password_manager.background_processing.FlutterBackgroundExecutor.Companion.CALLBACK_HANDLE_KEY
import java.util.*
import java.util.concurrent.CountDownLatch

class MessageService : JobIntentService() {

    override fun onCreate() {
        super.onCreate()
        if (backgroundExecutor == null) {
            backgroundExecutor = FlutterBackgroundExecutor()
        }
        backgroundExecutor?.startBackgroundIsolate(applicationContext)
    }

    /**
     * Called when word was added via [enqueueAlarmProcessing]
     * Calls flutter methods if [FlutterBackgroundExecutor] is running
     * or queues the work
     */
    override fun onHandleWork(intent: Intent) {
        if (backgroundExecutor?.isRunning == false) {
            Log.i(TAG, "AlarmService has not yet started.")
            addWorkToQueue(intent)
        } else {
            val latch = CountDownLatch(1)
            Handler(mainLooper).post {
                backgroundExecutor?.callFlutterMethod(latch, intent)
            }
            try {
                latch.await()
            } catch (ex: InterruptedException) {
                Log.i(TAG, "Exception waiting to execute Dart callback", ex)
            }
        }

    }

    private fun addWorkToQueue(intent: Intent) {
        synchronized(alarmQueue) {
            alarmQueue.add(intent)
            return
        }
    }

    companion object {
        private const val TAG = "MessageService"
        const val SHARED_PREFERENCES_KEY = "io.flutter.android_alarm_manager_plugin"
        private const val JOB_ID = 1984 // Random job ID.
        private val alarmQueue = Collections.synchronizedList(LinkedList<Intent>())
        private var backgroundExecutor: FlutterBackgroundExecutor? = null

        fun enqueueAlarmProcessing(context: Context?, alarmContext: Intent?) {
            context?.let {
                alarmContext?.let { alarmContext ->
                    enqueueWork(it, MessageService::class.java, JOB_ID, alarmContext)
                }
            }
        }

        /**
         * Called when the background executor was successfully started.
         * -> Process queue
         */
        fun onInitialized() {
            Log.i(TAG, "AlarmService started!")
            synchronized(alarmQueue) {
                for (intent in alarmQueue) {
                    backgroundExecutor?.callFlutterMethod(null, intent)
                }
                alarmQueue.clear()
            }
        }
    }
}