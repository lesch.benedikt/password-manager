package com.vgmmedia.flutter.password_manager.autofill

import android.view.autofill.AutofillId
import com.vgmmedia.flutter.password_manager.autofill.AutofillHeuristic

data class MatchedField(val heuristic: AutofillHeuristic, val autofillId: AutofillId, val value: String)