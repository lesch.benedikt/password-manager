package com.vgmmedia.flutter.password_manager.autofill

import android.annotation.TargetApi
import android.app.assist.AssistStructure
import android.os.Build
import android.view.View
import android.view.autofill.AutofillId
import androidx.annotation.RequiresApi

data class WebDomain(val scheme: String?, val domain: String)

@RequiresApi(Build.VERSION_CODES.M)
class AssistStructureParser(structure: AssistStructure) {

    private val autoFillIds = mutableListOf<AutofillId>()
    private val allNodes = mutableListOf<AssistStructure.ViewNode>()

    var packageNames = HashSet<String>()
    var webDomains = HashSet<WebDomain>()

    val fieldIds =
        mutableMapOf<AutofillInputType, MutableList<MatchedField>>()

    init {
        traverseStructure(structure)
    }

    private fun traverseStructure(structure: AssistStructure) {
        val windowNodes =
            (0 until structure.windowNodeCount).map {
                structure.getWindowNodeAt(it)
            }

        windowNodes.forEach { windowNode ->
            windowNode.rootViewNode?.let {
                traverseNode(it)
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun traverseNode(viewNode: AssistStructure.ViewNode) {
        allNodes.add(viewNode)

        viewNode.autofillId?.let { autoFillIds.add(it) }

        viewNode.idPackage?.let { packageNames.add(it) }

        viewNode.webDomain?.let { myWebDomain ->
            webDomains.add(
                WebDomain(
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        viewNode.webScheme
                    } else {
                        null
                    }, myWebDomain
                )
            )
        }

        viewNode.autofillId?.let { autofillId ->
            AutofillInputType.values().forEach { type ->

                val entry = type.heuristics
                    .filter { viewNode.autofillType != View.AUTOFILL_TYPE_NONE }
                    .filter { it.predicate(viewNode) }
                    .map { MatchedField(it, autofillId, viewNode.text.toString()) }

                fieldIds.getOrPut(type) { mutableListOf() }.addAll(
                    entry
                )
            }
        }

        val children = (0 until viewNode.childCount).map {
            viewNode.getChildAt(it)
        }

        children.forEach { childNode ->
            traverseNode(childNode)
        }
    }

    override fun toString(): String {
        return "AssistStructureParser(autoFillIds=$autoFillIds, packageName=$packageNames, webDomain=$webDomains, fieldIds=$fieldIds)"
    }
}