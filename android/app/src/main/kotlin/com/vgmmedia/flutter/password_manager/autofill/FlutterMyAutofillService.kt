package com.vgmmedia.flutter.password_manager.autofill

import android.app.assist.AssistStructure
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.CancellationSignal
import android.service.autofill.*
import android.util.Log
import android.view.autofill.AutofillId
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import com.vgmmedia.flutter.password_manager.SavedRequest
import com.vgmmedia.flutter.password_manager.background_processing.MessageService
import org.json.JSONException
import java.util.concurrent.atomic.AtomicBoolean

var savedCallback: FillCallback? = null
var savedPackage: String? = null
var savedParser: AssistStructureParser? = null
var savedRequest: SavedRequest? = null

@RequiresApi(api = Build.VERSION_CODES.O)
class FlutterMyAutofillService : AutofillService() {

    private var unlockLabel = "Autofill"

    override fun onConnected() {
        super.onConnected()
        val self = ComponentName(this, javaClass)

        val metaData = packageManager.getServiceInfo(self, PackageManager.GET_META_DATA).metaData
        metaData.getString("design.codeux.autofill_service.unlock_label")?.let {
            unlockLabel = it
        }
    }

    override fun onFillRequest(
        request: FillRequest,
        cancellationSignal: CancellationSignal,
        callback: FillCallback
    ) {
        Log.d("Autofill", "onFillRequest called")
        val context = request.fillContexts.last()
        val parser = AssistStructureParser(context.structure)

        savedCallback = callback
        savedPackage = packageName
        savedParser = parser

        try {
            if (parser.fieldIds.isNotEmpty()) {
                Log.d("Autofill", "Found field:" + parser.fieldIds.toString())

                var login: AutofillId? = null
                var password: AutofillId? = null
                if (parser.fieldIds[AutofillInputType.UserName]?.isNotEmpty() == true) {
                    login =
                        parser.fieldIds[AutofillInputType.UserName]?.first()?.autofillId
                }

                if (parser.fieldIds[AutofillInputType.Email]?.isNotEmpty() == true) {
                    login =
                        parser.fieldIds[AutofillInputType.Email]?.first()?.autofillId
                }

                if (parser.fieldIds[AutofillInputType.Password]?.isNotEmpty() == true) {
                    password =
                        parser.fieldIds[AutofillInputType.Password]?.first()?.autofillId
                }

                if (login != null && password != null) {
                    savedRequest = SavedRequest(
                        login,
                        password,
                        applicationContext.packageName,
                        AtomicBoolean(false)
                    )

                    val gson = Gson()

                    val alarmContext = Intent("Search")
                    alarmContext.putExtra("packageNames", gson.toJson(parser.packageNames))
                    alarmContext.putExtra(
                        "domains",
                        gson.toJson(parser.webDomains.map { it.domain })
                    )

                    MessageService.enqueueAlarmProcessing(applicationContext, alarmContext)
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    override fun onSaveRequest(request: SaveRequest, callback: SaveCallback) {
        val context: List<FillContext> = request.fillContexts
        val structure: AssistStructure = context[context.size - 1].structure
        val parser = AssistStructureParser(structure)

        val gson = Gson()

        val alarmContext = Intent("Save")
        alarmContext.putExtra("login", parser.fieldIds[AutofillInputType.Email]?.first()?.value)
        alarmContext.putExtra(
            "password",
            parser.fieldIds[AutofillInputType.Password]?.first()?.value
        )
        alarmContext.putExtra("packageNames", gson.toJson(parser.packageNames))
        alarmContext.putExtra("domains", gson.toJson(parser.webDomains.map { it.domain }))

        MessageService.enqueueAlarmProcessing(applicationContext, alarmContext)

        callback.onSuccess()
    }
}