package com.vgmmedia.flutter.password_manager

import android.content.Context
import android.util.Log
import androidx.core.content.edit
import com.vgmmedia.flutter.password_manager.background_processing.FlutterBackgroundExecutor
import com.vgmmedia.flutter.password_manager.background_processing.MessageService
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

class MainActivity : FlutterActivity() {
    private val CHANNEL = "com.vgmmedia.flutter.expenses/wear"

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { call, result ->
            val arguments = call.arguments

            when (call.method) {
                "start" -> {
                    Log.d("MainActivity", "start")
                    val callbackHandle = arguments as Long
                    setCallbackDispatcher(context, callbackHandle)
                    result.success(true)
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }

    private fun setCallbackDispatcher(context: Context?, callbackHandle: Long) {
        val prefs = context?.getSharedPreferences(MessageService.SHARED_PREFERENCES_KEY, 0)
        prefs?.edit {
            putLong(FlutterBackgroundExecutor.CALLBACK_HANDLE_KEY, callbackHandle)
        }
    }

}
