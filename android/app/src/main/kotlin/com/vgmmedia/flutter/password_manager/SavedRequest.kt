package com.vgmmedia.flutter.password_manager

import android.view.autofill.AutofillId
import java.util.concurrent.atomic.AtomicBoolean

data class SavedRequest(
    var login: AutofillId,
    var password: AutofillId,
    var ownPackageName: String?,
    var alreadyCalled: AtomicBoolean = AtomicBoolean(false)
) {

    override fun toString(): String {
        return "SavedRequest(userName=$login, password=$password, ownPackageName=$ownPackageName)"
    }
}