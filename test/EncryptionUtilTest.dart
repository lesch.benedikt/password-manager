import 'dart:convert';
import 'dart:typed_data';

import 'package:kee_pass_manager/entry_list/domain/entities/database.dart';
import 'package:kee_pass_manager/entry_list/domain/entities/group_entry.dart';
import 'package:kee_pass_manager/utils/encyption_util.dart';
import 'package:test/test.dart';
import 'package:uuid/uuid.dart';

void main() {
  test('Counter value should be incremented', () async {
    var database = Database(
        path: "",
        root: GroupEntry(Uuid().v4(), "TestTitle", null, List.empty()));
    Uint8List encypted = await EncryptionUtil.encryptDb(database, "TestPass");
    String decrypted = await EncryptionUtil.decryptDb(encypted, "TestPass");

    var jsonDecode2 = jsonDecode(decrypted) as Map<String, dynamic>;
    expect(jsonDecode2, database.toJson());
  });
}
